// WARNING: THE USAGE OF CUSTOM SCRIPTS IS NOT SUPPORTED. VTEX IS NOT LIABLE FOR ANY DAMAGES THIS MAY CAUSE. THIS MAY BREAK YOUR STORE AND STOP SALES. IN CASE OF ERRORS, PLEASE DELETE THE CONTENT OF THIS

$(function(){
	$('.footer__selos').html(`
        <li class="list-inline-item selos__item selos__item--ebit"><a id="seloEbit" href="http://www.ebit.com.br/#enrolado-tecidos" target="_blank" onclick="redir(this.href);"><img src="https://newimgebit-a.akamaihd.net/ebitBR/selo/img_76709.png" alt="" /></a></li>
        <li class="list-inline-item selos__item selos__item--site-protegido"><img src="/arquivos/selo__site-protegido.png" alt="" /></li>
        <li class="list-inline-item selos__item selos__item--safe-browsing"><a href="https://transparencyreport.google.com/safe-browsing/search?url=www.enroladotecidos.com.br" target="_blank"><img src="/arquivos/selo__safe-browsing.png" alt="" /></a></li>
        <li class="list-inline-item selos__item selos__item--loja-confiavel"><a href="https://www.lojaconfiavel.com/enroladotecidos" class="ts-footerstamp" data-lcname="enroladotecidos"  target="_blank"><img src="//service.yourviews.com.br/Image/4790dc80-47b3-466f-b394-82d3feb57bc6/Footer.jpg" title="Loja Confiável" alt="Loja Confiável" style="width: 88px; height: 91px;"/></a></li>
        <li class="list-inline-item selos__item selos__item--vtex-pci"><img src="/arquivos/selo__vtex-pci.png" alt="" /></li>
    `);
}); 

window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?3tyhMpr4c4QtcFWzAhCVQjqE75hDwWRp";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");


var CLIENT_TOKEN = '09112017103951ZTT';
var body = document.getElementsByTagName('body')[0];
var script = document.createElement('script');
script.type = 'text/javascript';
script.onload = function() {};
script.src = 'https://ca.enviou.com.br/js/ca-vtex.js';
body.appendChild(script);


var hideCheckoutButton = function(){
    if (vtexjs && $('.payment-confirmation-wrap').length && window.location.href.indexOf('payment') != -1) {
        clearInterval(checkBlacklist);

        vtexjs.checkout.getOrderForm()
        .then(function(orderForm){           
            if (orderForm) {
                if (orderForm.clientProfileData.email) {
                    let email = orderForm.clientProfileData.email;
                    
                    $.ajax({
                        crossDomain: true,
                        url: "/api/dataentities/BL/search?email=" + email,
                        method: "GET",
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/vnd.vtex.ds.v10+json",
                            "REST-Range": "resources=0-49",
                            "X-VTEX-API-AppKey": "vtexappkey-enroladotecidos-KVZFFN",
                            "X-VTEX-API-AppToken": "PJYUHXZOWTMZGTPAHTZOURAOONKQSUZGMNKDFPSYVFQSDODPVHQRKGKYIKFTMIWCTFMSIMVSBASOBIKAJVVKDIWFCMVVKZLMJNZYCLHJMYQJOVWZZFJRNMZKJGPVQAIB"
                        }
                    }).then(function(response) {
                        if (response.length) {
                            console.log('client is blacklisted, hide checkout button');
                            $('.payment-confirmation-wrap').remove();
                        } else {
                            console.log('client is not blacklisted, no need to hide checkout button');
                        }
                    }).fail(function(response){
                        console.log(response);
                    })
                }
            }
        })
    };
};

var checkBlacklist = setInterval(hideCheckoutButton, 100);



var limitAddressComplement = function(){
    if ($('#ship-more-info').length) {
        clearInterval(checkAddressInfo);

        $('#ship-more-info')
            .attr('maxlength', 30)
            .attr('data-parsley-maxlength', 30)
    };
};

var checkAddressInfo = setInterval(limitAddressComplement, 100);


var limitShipNumber = function(){
    if ($('#ship-number').length) {
        clearInterval(checkShipNumber);

        $('#ship-number')
            .attr('maxlength', 5)
    };
};

var checkShipNumber = setInterval(limitShipNumber, 100);


var limitNeighborhood = function(){
    if ($('#ship-neighborhood').length) {
        clearInterval(checkNeighborhood);

        $('#ship-neighborhood')
            .attr('maxlength', 30)
	        .attr('data-parsley-maxlength', 30)
    };
};

var checkNeighborhood = setInterval(limitNeighborhood, 100);