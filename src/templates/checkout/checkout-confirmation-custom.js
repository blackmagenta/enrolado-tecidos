// WARNING: THE USAGE OF CUSTOM SCRIPTS IS NOT SUPPORTED. VTEX IS NOT LIABLE FOR ANY DAMAGES THIS MAY CAUSE.
// THIS MAY BREAK YOUR STORE AND STOP SALES. IN CASE OF ERRORS, PLEASE DELETE THE CONTENT OF THIS SCRIPT.

var showDepositInfo = function(){
    if ($('.cconf-payment').length) {
        clearInterval(checkDeposit);

        if ($("span:contains('Deposito Bancário Banco do Brasil')").length || $("span:contains('Deposito Bancário Bradesco')").length) {
            $('.deposit-info').show();
    
            if ($("span:contains('Deposito Bancário Banco do Brasil')").length) {
                $('.deposit-info__bank--bb').show();
            };
            
            if ($("span:contains('Deposito Bancário Bradesco')").length) {
                $('.deposit-info__bank--bradesco').show();
            };
        };
    };
};

var checkDeposit = setInterval(showDepositInfo, 100);