requirejs.config({
    paths: {
        jqueryui: '../vendor/jquery-ui-1.10.4.custom.min',
        jqueryuitouch: '../vendor/jquery.ui.touch-punch.min',
        flexslider: '../vendor/jquery.flexslider-min',
        bodymovin: '../vendor/bodymovin.min',
        helpers: '../helpers'
    },
    shim: {
        jqueryuitouch: ['jqueryui']
    }
});