define(function(){
    return {
        items: [
            {
                id: '1078',
                name: "Sousplat em Tecido Jacquard Estampado Azulejo Português Marsala",
                imageUrl: "http://enroladotecidos.vteximg.com.br/arquivos/ids/156369-55-55/sousplat-tecido-jacquard-estampado-azulejo-portugues-marsala.jpg?v=636518261713670000",
                price: 1000, 
                sellingPrice: 960,
                quantity: 12,
                detailUrl: '/sousplat-tecido-jacquard-estampado-azulejo-portugues-marsala-jpg/p'
            },
            {
                id: '1078',
                name: "Sousplat em Tecido Jacquard Estampado Azulejo Português Marsala",
                imageUrl: "http://enroladotecidos.vteximg.com.br/arquivos/ids/156369-55-55/sousplat-tecido-jacquard-estampado-azulejo-portugues-marsala.jpg?v=636518261713670000",
                price: 1000, 
                sellingPrice: 1000,
                quantity: 1,
                detailUrl: '/sousplat-tecido-jacquard-estampado-azulejo-portugues-marsala-jpg/p'
            }
        ],
        shippingData: {
            address: {
                postalCode: '13469-571'
            },
            logisticsInfo: [
                {
                    selectedSla: 'Normal',
                    slas: [
                        {
                            id: 'Normal',
                            name: 'Normal',
                            price: 333,
                            shippingEstimate: '5bd'
                        },
                        {
                            id: 'Expressa',
                            name: 'Expressa',
                            price: 500,
                            shippingEstimate: '3bd'
                        }
                    ]
                }
            ],
        },
        totalizers: [
            {
                id: 'Items',
                name: 'Total de Items',
                value: 2000
            },
            {
                id: 'Discounts',
                name: 'Desconto',
                value: -500
            },
            {
                id: 'Shipping',
                name: 'Frete',
                value: 500
            }
        ],
        value: 2000
    };
});