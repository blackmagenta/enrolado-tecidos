define(function(){
    return {
        menu: [
            {
                name: "Tecidos para decoração",
                link: "/tecidos-para-decoracao",
                children: [
                    {
                        name: "Tecidos para festas infantis",
                        link: "/tecidos-para-decoracao/festas-infantis",
                        children: [
                            {
                                name: "Party Decor",
                                link: "/tecidos-para-decoracao/festas-infantis/party-decor"
                            },
                            {
                                name: "Gorgurinho",
                                link: "/tecidos-para-decoracao/festas-infantis/gorgurinho"
                            },
                            {
                                name: "Jacquard Tradicional",
                                link: "/tecidos-para-decoracao/festas-infantis/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Estampado",
                                link: "/tecidos-para-decoracao/festas-infantis/jacquard-estampado"
                            },
                            {
                                name: "Jacquard Fio Brilhante",
                                link: "/tecidos-para-decoracao/festas-infantis/jacquard-fio-brilhante"
                            },
                            {
                                name: "Jacquard Lurex",
                                link: "/tecidos-para-decoracao/festas-infantis/jacquard-fio-lurex"
                            },
                            {
                                name: "Jacquard Luxo",
                                link: "/tecidos-para-decoracao/festas-infantis/jacquard-luxo"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/tecidos-para-decoracao/festas-infantis/jacquard-fio-tinto"
                            },
                            {
                                name: "Brugges",
                                link: "/tecidos-para-decoracao/festas-infantis/brugges"
                            },
                            {
                                name: "Cetim",
                                link: "/tecidos-para-decoracao/festas-infantis/cetim"
                            },
                            {
                                name: "Oxford",
                                link: "/tecidos-para-decoracao/festas-infantis/oxford"
                            },
                            {
                                name: "Oxford Xadrez",
                                link: "/tecidos-para-decoracao/festas-infantis/oxford-xadrez"
                            },
                            {
                                name: "Tricoline",
                                link: "/tecidos-para-decoracao/festas-infantis/tricoline"
                            },
                            {
                                name: "Voil",
                                link: "/tecidos-para-decoracao/festas-infantis/voil"
                            },
                            {
                                name: "Tule",
                                link: "/tecidos-para-decoracao/festas-infantis/tule"
                            },
                            {
                                name: "Shantung",
                                link: "/tecidos-para-decoracao/festas-infantis/shantung"
                            }
                        ]
                    },
                    {
                        name: "Tecidos para cortinas",
                        link: "/tecidos-para-decoracao/cortinas",
                        children: [
                            {
                                name: "Voil",
                                link: "/tecidos-para-decoracao/cortinas/voil"
                            },
                            {
                                name: "Voil Flame",
                                link: "/tecidos-para-decoracao/cortinas/voil-flame"
                            },
                            {
                                name: "Blackout",
                                link: "/tecidos-para-decoracao/cortinas/blackout"
                            },
                            {
                                name: "Linho",
                                link: "/tecidos-para-decoracao/cortinas/linho"
                            },
                            {
                                name: "Brugges",
                                link: "/tecidos-para-decoracao/cortinas/brugges"
                            },
                            {
                                name: "Jacquard Tradicional",
                                link: "/tecidos-para-decoracao/cortinas/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Fio Brilhante",
                                link: "/tecidos-para-decoracao/cortinas/jacquard-fio-brilhante"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/tecidos-para-decoracao/cortinas/jacquard-fio-tinto"
                            },
                            {
                                name: "Jacquard Lurex",
                                link: "/tecidos-para-decoracao/cortinas/jacquard-fio-lurex"
                            },
                            {
                                name: "Jacquard Luxo",
                                link: "/tecidos-para-decoracao/cortinas/jacquard-luxo"
                            },
                            {
                                name: "Oxford",
                                link: "/tecidos-para-decoracao/cortinas/oxford"
                            },
                            {
                                name: "Renda",
                                link: "/tecidos-para-decoracao/cortinas/renda"
                            },
                            {
                                name: "Shantung",
                                link: "/tecidos-para-decoracao/cortinas/shantung"
                            },
                            // {
                            //     name: "Blackout 100%",
                            //     link: "/tecidos-para-decoracao/cortinas/blackout-100"
                            // }
                        ]
                    },
                    {
                        name: "Tecidos para paredes",
                        link: "/tecidos-para-decoracao/paredes",
                        children: [
                            {
                                name: "Jacquard Tradicional",
                                link: "/tecidos-para-decoracao/paredes/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Luxo",
                                link: "/tecidos-para-decoracao/paredes/jacquard-luxo"
                            },
                            {
                                name: "Jacquard Estampado",
                                link: "/tecidos-para-decoracao/paredes/jacquard-estampado"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/tecidos-para-decoracao/paredes/jacquard-fio-tinto"
                            },
                            {
                                name: "Jacquard Lurex",
                                link: "/tecidos-para-decoracao/paredes/jacquard-fio-lurex"
                            },
                            {
                                name: "Linha Summer",
                                link: "/tecidos-para-decoracao/paredes/linha-summer"
                            },
                            {
                                name: "Linha D'acqua",
                                link: "/tecidos-para-decoracao/paredes/linha-d-acqua"
                            },
                            {
                                name: "Wall Linea",
                                link: "/tecidos-para-decoracao/paredes/wall-linea"
                            },
                            {
                                name: "Acqua Linea",
                                link: "/tecidos-para-decoracao/paredes/acqua-linea"
                            },
                        ]
                    },
                    {
                        name: "Tecidos para toalhas de mesa",
                        link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa",
                        children: [
                            {
                                name: "Jacquard Tradicional",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/jacquard-fio-tinto"
                            },
                            {
                                name: "Jacquard Estampado",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/jacquard-estampado"
                            },
                            {
                                name: "Jacquard Luxo",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/jacquard-luxo"
                            },
                            {
                                name: "Jacquard Fio Brilhante",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/jacquard-fio-brilhante"
                            },
                            {
                                name: "Jacquard Lurex",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/jacquard-fio-lurex"
                            },
                            {
                                name: "Oxford",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/oxford"
                            },
                            {
                                name: "Oxford Xadrez",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/oxford-xadrez"
                            },
                            {
                                name: "Gorgurinho",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/gorgurinho"
                            },
                            {
                                name: "Linho",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/linho"
                            },
                            {
                                name: "Cetim",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/cetim"
                            },
                            {
                                name: "Renda",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/renda"
                            },
                            {
                                name: "Brugges",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/brugges"
                            },
                            {
                                name: "Shantung",
                                link: "/tecidos-para-decoracao/tecidos-para-toalhas-de-mesa/shantung"
                            }
                        ]
                    },
                    {
                        name: "Tecidos para roupas de cama",
                        link: "/tecidos-para-decoracao/roupas-de-cama",
                        children: [
                            {
                                name: "Percal",
                                link: "/tecidos-para-decoracao/roupas-de-cama/percal"
                            },
                            {
                                name: "Tricoline",
                                link: "/tecidos-para-decoracao/roupas-de-cama/tricoline"
                            },
                        ]
                    },
                    {
                        name: "Tecidos para confecção de brindes",
                        link: "/tecidos-para-decoracao/confeccoes-de-brindes",
                        children: [
                            {
                                name: "Neoplex",
                                link: "/tecidos-para-decoracao/confeccoes-de-brindes/neoplex"
                            },
                            {
                                name: "Oxford Xadrez",
                                link: "/tecidos-para-decoracao/confeccoes-de-brindes/oxford-xadrez"
                            },
                            {
                                name: "Tricoline",
                                link: "/tecidos-para-decoracao/confeccoes-de-brindes/tricoline"
                            },
                        ]
                    },
                    {
                        name: "Acessórios",
                        link: "/acessorios",
                        children: [
                            {
                                name: "Mostruários e Cartelas de amostras",
                                link: "/acessorios/mostruarios-e-cartelas-de-amostras"
                            },
                            {
                                name: "Colas e Impermeabilizantes",
                                link: "/acessorios/colas-e-impermeabilizantes"
                            },
                            {
                                name: "Sousplat MDF",
                                link: "/acessorios/sousplat-mdf"
                            },
                        ]
                    }
                ]
            },
            {
                name: "Tecidos para móveis",
                link: "/tecidos-para-moveis",
                children: [
                    {
                        name: "Tecidos para capas de móveis",
                        link: "/tecidos-para-moveis/capas-de-moveis",
                        children: [
                            {
                                name: "Jacquard Tradicional",
                                link: "/tecidos-para-moveis/capas-de-moveis/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Estampado",
                                link: "/tecidos-para-moveis/capas-de-moveis/jacquard-estampado"
                            },
                            {
                                name: "Jacquard Luxo",
                                link: "/tecidos-para-moveis/capas-de-moveis/jacquard-luxo"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/tecidos-para-moveis/capas-de-moveis/jacquard-fio-tinto"
                            },
                            {
                                name: "Jacquard Fio Brilhante",
                                link: "/tecidos-para-moveis/capas-de-moveis/jacquard-fio-brilhante"
                            },
                            {
                                name: "Gorgurinho",
                                link: "/tecidos-para-moveis/capas-de-moveis/gorgurinho"
                            },
                            {
                                name: "Pied Poule",
                                link: "/tecidos-para-moveis/capas-de-moveis/pied-poule"
                            },
                            {
                                name: "Linho",
                                link: "/tecidos-para-moveis/capas-de-moveis/linho"
                            },
                            {
                                name: "Sarja",
                                link: "/tecidos-para-moveis/capas-de-moveis/sarja"
                            },
                             {
                                 name: "Shantung",
                                 link: "/tecidos-para-moveis/capas-de-moveis/shantung"
                             }
                        ]
                    },
                    {
                        name: "Cadeiras",
                        link: "/tecidos-para-moveis/cadeiras",
                        children: [
                            {
                                name: "Suede Liso",
                                link: "/tecidos-para-moveis/cadeiras/suede-liso"
                            },
                            {
                                name: "Suede Amassado",
                                link: "/tecidos-para-moveis/cadeiras/suede-amassado"
                            },
                            {
                                name: "Suede Animale",
                                link: "/tecidos-para-moveis/cadeiras/suede-animale"
                            },
                            {
                                name: "Suede Pena",
                                link: "/tecidos-para-moveis/cadeiras/suede-pena"
                            },
                            {
                                name: "Nobuck",
                                link: "/tecidos-para-moveis/cadeiras/nobuck"
                            },
                            {
                                name: "Corano",
                                link: "/tecidos-para-moveis/cadeiras/corano"
                            },
                            {
                                name: "Linho",
                                link: "/tecidos-para-moveis/cadeiras/linho"
                            },
                            {
                                name: "Linha Summer",
                                link: "/tecidos-para-moveis/cadeiras/linha-summer"
                            },
                            {
                                name: "Linha D'acqua",
                                link: "/tecidos-para-moveis/cadeiras/linha-d-acqua"
                            },
                            {
                                name: "Linen Look Rustik",
                                link: "/tecidos-para-moveis/cadeiras/linen-look"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/tecidos-para-moveis/cadeiras/jacquard-fio-tinto"
                            },
                            {
                                name: "Jacquard Estampado",
                                link: "/tecidos-para-moveis/cadeiras/jacquard-estampado"
                            },
                            {
                                name: "Sarja",
                                link: "/tecidos-para-moveis/cadeiras/sarja"
                            },
                            {
                                name: "Acqua Linea",
                                link: "/tecidos-para-decoracao/paredes/acqua-linea"
                            },
                        ]
                    },
                    {
                        name: "Móveis de piscina",
                        link: "/tecidos-para-moveis/moveis-de-piscina",
                        children: [
                            {
                                name: "Linha Summer",
                                link: "/tecidos-para-moveis/moveis-de-piscina/linha-summer"
                            },
                            {
                                name: "Linha D'acqua",
                                link: "/tecidos-para-moveis/moveis-de-piscina/linha-d-acqua"
                            },
                            {
                                name: "Acqua Linea",
                                link: "/tecidos-para-decoracao/paredes/acqua-linea"
                            },
                        ]
                    },
                    {
                        name: "Sofás e poltronas",
                        link: "/tecidos-para-moveis/sofas-e-poltronas",
                        children: [
                            {
                                name: "Corano",
                                link: "/tecidos-para-moveis/sofas-e-poltronas/corano"
                            },
                            {
                                name: "Sarja",
                                link: "/tecidos-para-moveis/sofas-e-poltronas/sarja"
                            },
                            {
                                name: "Suede Amassado",
                                link: "/tecidos-para-moveis/sofas-e-poltronas/suede-amassado"
                            },
                            {
                                name: "Suede Liso",
                                link: "/tecidos-para-moveis/sofas-e-poltronas/suede-liso"
                            },
                            {
                                name: "Suede Animale",
                                link: "/tecidos-para-moveis/sofas-e-poltronas/suede-animale"
                            },
                            {
                                name: "Suede Pena",
                                link: "/tecidos-para-moveis/sofas-e-poltronas/suede-pena"
                            },
                            {
                                name: "Nobuck",
                                link: "/tecidos-para-moveis/sofas-e-poltronas/nobuck"
                            },
                            {
                                name: "Linho",
                                link: "/tecidos-para-moveis/sofas-e-poltronas/linho"
                            },
                            {
                                name: "Linen Look Rustik",
                                link: "/tecidos-para-moveis/sofas-e-poltronas/linen-look"
                            },
                        ]
                    }
                ]
            },
            {
                name: "Tecidos para vestuário",
                link: "/tecidos-para-vestuario",
                children: [
                    {
                        name: "Calças e bermudas",
                        link: "/tecidos-para-vestuario/calcas-e-bermudas",
                        children: [
                            {
                                name: "Sarja",
                                link: "/tecidos-para-vestuario/calcas-e-bermudas/sarja"
                            },
                            {
                                name: "Oxford",
                                link: "/tecidos-para-vestuario/calcas-e-bermudas/oxford"
                            },
                        ]
                    },
                    {
                        name: "Regatas, batas e camisas",
                        link: "/tecidos-para-vestuario/regatas-batas-e-camisas",
                        children: [
                            {
                                name: "Cetim",
                                link: "/tecidos-para-vestuario/regatas-batas-e-camisas/cetim"
                            },
                            {
                                name: "Oxford",
                                link: "/tecidos-para-vestuario/regatas-batas-e-camisas/oxford"
                            },
                            {
                                name: "Viscose",
                                link: "/tecidos-para-vestuario/regatas-batas-e-camisas/viscose-absolut"
                            },
                            {
                                name: "Tricoline",
                                link: "/tecidos-para-vestuario/regatas-batas-e-camisas/tricoline"
                            },
                        ]
                    },
                    {
                        name: "Uniformes",
                        link: "/tecidos-para-vestuario/uniformes",
                        children: [
                            {
                                name: "Oxford",
                                link: "/tecidos-para-vestuario/uniformes/oxford"
                            },
                            {
                                name: "Oxford Xadrez",
                                link: "/tecidos-para-vestuario/uniformes/oxford-xadrez"
                            },
                            {
                                name: "Pied Poule",
                                link: "/tecidos-para-vestuario/uniformes/pied-poule"
                            },
                        ]
                    },
                    {
                        name: "Vestidos",
                        link: "/tecidos-para-vestuario/vestidos",
                        children: [
                            {
                                name: "Cetim",
                                link: "/tecidos-para-vestuario/vestidos/cetim"
                            },
                            // {
                            //     name: "Tule",
                            //     link: "/tecidos-para-vestuario/vestidos/tule"
                            // },
                        ]
                    }
                ]
            },
            {
                name: "Eventos",
                link: "/eventos",
                children: [
                    {
                        name: "Carpetes e passadeiras",
                        link: "/eventos/carpetes-e-passadeiras",
                        children: []
                    },
                    {
                        name: "Guardanapos",
                        link: "/eventos/guardanapos-para-eventos",
                        children: [
                            {
                                name: "Guardanapos em Oxford",
                                link: "/eventos/guardanapos-para-eventos/oxford"
                            },
                            {
                                name: "Guardanapos em Jacquard Tradicional",
                                link: "/eventos/guardanapos-para-eventos/jacquard-tradicional"
                            },
                            {
                                name: "Guardanapos em Linho",
                                link: "/eventos/guardanapos-para-eventos/linho"
                            },
                        ]
                    },
                    {
                        name: "Sousplat",
                        link: "/eventos/sousplat",
                        children: [
                            {
                                name: "Capas em Jacquard Tradicional",
                                link: "/eventos/sousplat/jacquard-tradicional"
                            },
                            {
                                name: "Capas em Jacquard Estampado",
                                link: "/eventos/sousplat/jacquard-estampado"
                            },
                        ]
                    },
                    {
                        name: "Tecidos para eventos",
                        link: "/eventos/tecidos-para-eventos",
                        children: [
                            {
                                name: "Jacquard Tradicional",
                                link: "/eventos/tecidos-para-eventos/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Estampado",
                                link: "/eventos/tecidos-para-eventos/jacquard-estampado"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/eventos/tecidos-para-eventos/jacquard-fio-tinto"
                            },
                            {
                                name: "Jacquard Luxo",
                                link: "/eventos/tecidos-para-eventos/jacquard-luxo"
                            },
                            {
                                name: "Jacquard Lurex",
                                link: "/eventos/tecidos-para-eventos/jacquard-fio-lurex"
                            },
                            {
                                name: "Jacquard Fio Brilhante",
                                link: "/eventos/tecidos-para-eventos/jacquard-fio-brilhante"
                            },
                            {
                                name: "Brugges",
                                link: "/eventos/tecidos-para-eventos/brugges"
                            },
                            {
                                name: "Oxford",
                                link: "/eventos/tecidos-para-eventos/oxford"
                            },
                            {
                                name: "Gorgurinho",
                                link: "/eventos/tecidos-para-eventos/gorgurinho"
                            },
                            {
                                name: "Cetim",
                                link: "/eventos/tecidos-para-eventos/cetim"
                            },
                            {
                                name: "Linho",
                                link: "/eventos/tecidos-para-eventos/linho"
                            },
                            {
                                name: "Voil",
                                link: "/eventos/tecidos-para-eventos/voil"
                            },
                            {
                                name: "Party Decor",
                                link: "/eventos/tecidos-para-eventos/party-decor"
                            },
                            {
                                name: "Renda",
                                link: "/eventos/tecidos-para-eventos/renda"
                            },
                            {
                                name: "Shantung",
                                link: "/eventos/tecidos-para-eventos/shantung"
                            }
                            // {
                            //     name: "Tule",
                            //     link: "/eventos/tecidos-para-eventos/tule"
                            // }
                        ]
                    },
                    {
                        name: "Toalhas de Mesa",
                        link: "/eventos/toalhas",
                        children: [
                            {
                                name: "Jacquard Tradicional",
                                link: "/eventos/toalhas/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Estampado",
                                link: "/eventos/toalhas/jacquard-estampado"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/eventos/toalhas/jacquard-fio-tinto"
                            },
                            {
                                name: "Jacquard Lurex",
                                link: "/eventos/toalhas/jacquard-fio-lurex"
                            },
                            {
                                name: "Jacquard Fio Brilhante",
                                link: "/eventos/toalhas/jacquard-fio-brilhante"
                            },
                            {
                                name: "Oxford",
                                link: "/eventos/toalhas/oxford"
                            },
                            {
                                name: "Cetim",
                                link: "/eventos/toalhas/cetim"
                            },
                            {
                                name: "Jacquard Luxo",
                                link: "/eventos/toalhas/jacquard-luxo"
                            },
                            {
                                name: "Renda",
                                link: "/eventos/toalhas/renda"
                            },
                            {
                                name: "Shantung",
                                link: "/eventos/toalhas/shantung"
                            }
                        ]
                    }
                ]
            },
            {
                name: "Casa",
                link: "/casa",
                children: [
                    {
                        name: "Guardanapos",
                        link: "/casa/guardanapos",
                        children: [
                            {
                                name: "Guardanapos em Oxford",
                                link: "/casa/guardanapos/oxford"
                            },
                            {
                                name: "Guardanapos em Jacquard",
                                link: "/casa/guardanapos/jacquard-tradicional"
                            },
                            {
                                name: "Guardanapos em Linho",
                                link: "/casa/guardanapos/linho"
                            },
                        ]
                    },
                    {
                        name: "Capas para sousplat",
                        link: "/casa/capas-para-sousplat",
                        children: [ 
                            {
                                name: "Jacquard Tradicional",
                                link: "/casa/capas-para-sousplat/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Estampado",
                                link: "/casa/capas-para-sousplat/jacquard-estampado"
                            },
                        ]
                    },
                    {
                        name: "Caminha PET",
                        link: "/casa/caminha-pet",
                        children: []
                    },  
                    {
                        name: "Cortinas em Jacquard",
                        link: "/casa/cortinas/jacquard-tradicional",
                        children: []
                    },
                    {
                        name: "Tecidos para parede",
                        link: "/casa/tecidos-para-parede",
                        children: [
                            {
                                name: "Jacquard Tradicional",
                                link: "/casa/tecidos-para-parede/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Luxo",
                                link: "/casa/tecidos-para-parede/jacquard-luxo"
                            },
                            {
                                name: "Jacquard Estampado",
                                link: "/casa/tecidos-para-parede/jacquard-estampado"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/casa/tecidos-para-parede/jacquard-fio-tinto"
                            },
                            {
                                name: "Jacquard Lurex",
                                link: "/casa/tecidos-para-parede/jacquard-fio-lurex"
                            },
                            {
                                name: "Linha Summer",
                                link: "/casa/tecidos-para-parede/linha-summer"
                            },
                            {
                                name: "Linha D'acqua",
                                link: "/casa/tecidos-para-parede/linha-d-acqua"
                            },
                            {
                                name: "Wall Linea",
                                link: "/casa/tecidos-para-parede/wall-linea"
                            },
                            {
                                name: "Acqua Linea",
                                link: "/tecidos-para-decoracao/paredes/acqua-linea"
                            },
                        ]
                    },
                    {
                        name: "Mantas e xales para sofás",
                        link: "/casa/manta",
                        children: [
                            {
                                name: "Jacquard Tradicional",
                                link: "/casa/manta/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/casa/manta/jacquard-fio-tinto"
                            },
                            {
                                name: "Rústicas",
                                link: "/casa/manta/manta-rustica"
                            },
                        ]
                    },
                    {
                        name: "Almofadas",
                        link: "/casa/almofadas",
                        children: [
                            {
                                name: "Jacquard Tradicional",
                                link: "/casa/almofadas/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Estampado",
                                link: "/casa/almofadas/jacquard-estampado"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/casa/almofadas/jacquard-fio-tinto"
                            },
                            {
                                name: "Suede Amassado",
                                link: "/casa/almofadas/suede-amassado"
                            },
                            {
                                name: "Enchimentos",
                                link: "/casa/almofadas/enchimento"
                            },
                        ]
                    },
                    {
                        name: "Toalhas de Mesa",
                        link: "/eventos/toalhas",
                        children: [
                            {
                                name: "Jacquard Tradicional",
                                link: "/eventos/toalhas/jacquard-tradicional"
                            },
                            {
                                name: "Jacquard Estampado",
                                link: "/eventos/toalhas/jacquard-estampado"
                            },
                            {
                                name: "Jacquard Fio Tinto",
                                link: "/eventos/toalhas/jacquard-fio-tinto"
                            },
                            {
                                name: "Jacquard Lurex",
                                link: "/eventos/toalhas/jacquard-fio-lurex"
                            },
                            {
                                name: "Jacquard Fio Brilhante",
                                link: "/eventos/toalhas/jacquard-fio-brilhante"
                            },
                            {
                                name: "Oxford",
                                link: "/eventos/toalhas/oxford"
                            },
                            {
                                name: "Cetim",
                                link: "/eventos/toalhas/cetim"
                            },
                            {
                                name: "Jacquard Luxo",
                                link: "/eventos/toalhas/jacquard-luxo"
                            },
                            {
                                name: "Renda",
                                link: "/eventos/toalhas/renda"
                            },
                            {
                                name: "Shantung",
                                link: "/eventos/toalhas/shantung"
                            }
                        ]
                    },
                    {
                        name: "Tecidos para roupas de cama",
                        link: "/casa/tecidos-para-roupas-de-cama",
                        children: [
                            {
                                name: "Percal",
                                link: "/casa/tecidos-para-roupas-de-cama/percal"
                            },
                        ]
                    },
                    {
                        name: "Acessórios",
                        link: "/acessorios",
                        children: [
                            {
                                name: "Mostruários e Cartelas de amostras",
                                link: "/acessorios/mostruarios-e-cartelas-de-amostras"
                            },
                            {
                                name: "Colas e Impermeabilizantes",
                                link: "/acessorios/colas-e-impermeabilizantes"
                            },
                            {
                                name: "Sousplat MDF",
                                link: "/acessorios/sousplat-mdf"
                            },
                        ]
                    }
                ]
            },
            {
                name: "Patchwork",
                link: "/patchwork",
                children: [
                    {
                        name: "Tecidos para patchwork",
                        link: "/patchwork/tecidos-para-patchwork",
                        children: [
                            {
                                name: "Tricoline",
                                link: "/patchwork/tecidos-para-patchwork/tricoline"
                            },
                            {
                                name: "Pied Poule",
                                link: "/patchwork/tecidos-para-patchwork/pied-poule"
                            },
                            {
                                name: "Oxford Xadrez",
                                link: "/patchwork/tecidos-para-patchwork/oxford-xadrez"
                            },
                            {
                                name: "Viscose Absolut",
                                link: "/patchwork/tecidos-para-patchwork/viscose-absolut"
                            },
                            // {
                            //     name: "Tule",
                            //     link: "/patchwork/tecidos-para-patchwork/tule"
                            // },
                        ]
                    },
                    {
                        name: "Acessórios",
                        link: "/acessorios",
                        children: [
                            {
                                name: "Mostruários e Cartelas de amostras",
                                link: "/acessorios/mostruarios-e-cartelas-de-amostras"
                            },
                            {
                                name: "Colas e Impermeabilizantes",
                                link: "/acessorios/colas-e-impermeabilizantes"
                            },
                            // {
                            //     name: "Sousplat MDF",
                            //     link: "/acessorios/sousplat-mdf"
                            // },
                        ]
                    }
                ]
            }
        ]
    }
});