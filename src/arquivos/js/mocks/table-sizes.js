define(function(){
    return [
        {
            name: 'Toalha redonda de buffet',
            value: 'redonda-buffet',
            sizes: [
                {
                    seats: '6',
                    meters: '2,8'
                },
                {
                    seats: '6-8',
                    meters: '2,9'
                },
                {
                    seats: '8',
                    meters: '3,0'
                },
                {
                    seats: '8-10',
                    meters: '3,2'
                },
                {
                    seats: '10-12',
                    meters: '3,2'
                }
            ]
        },
        {
            name: 'Toalha redonda residencial',
            value: 'redonda-residencial',
            sizes: [
                {
                    seats: '4',
                    meters: '1,4'
                },
                {
                    seats: '6',
                    meters: '1,8'
                },
                {
                    seats: '8',
                    meters: '2,2'
                },
                {
                    seats: '10',
                    meters: '2,6'
                },
                {
                    seats: '12',
                    meters: '2,8'
                }
            ]
        },
        {
            name: 'Toalha quadrada',
            value: 'quadrada',
            sizes: [
                {
                    seats: '4',
                    meters: '1,4 x 1,4'
                },
                {
                    seats: '8',
                    meters: '2,2 x 2,2'
                },
                {
                    seats: '12',
                    meters: '2,8 x 2,8'
                }
            ]
        },
        {
            name: 'Toalha retangular',
            value: 'retangular',
            sizes: [
                {
                    seats: '6',
                    meters: '1,4 x 2,1'
                },
                {
                    seats: '8',
                    meters: '1,4 x 2,5'
                },
                {
                    seats: '10',
                    meters: '1,4 x 3,0'
                },
                {
                    seats: '12',
                    meters: '1,4 x 3,2'
                }
            ]
        }
    ];
})