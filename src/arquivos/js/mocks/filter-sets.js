define(function(){
    return {
        filterSets: [
            // todos os filtros - este set de filtros é utilizado caso a página sendo acessada não conter marca selecionada

            {
                brandIds: [],
                filters: [
                    {
                        id: 19,
                        name: 'Cor Predominante'
                    }, 
                    {
                        id: 20,
                        name: 'Tom Predominante'
                    }, 
                    {
                        id: 21,
                        name: 'Desenho'
                    }, 
                    {
                        id: 22,
                        name: 'Estampa'
                    }, 
                    {
                        id: 23,
                        name: 'Orientação da estampa'
                    }, 
                    {
                        id: 24,
                        name: 'Sugestão de aplicação'
                    }, 
                    {
                        id: 25,
                        name: 'Largura do tecido'
                    }, 
                    {
                        id: 26,
                        name: 'Pré-Lavagem'
                    }
                ]
            },

            // os filtros abaixo são validos para todas as marcas com ids inseridos na array brandIds

            // Jacquard Tradicional - Fio Brilhante - Fio Lurex - Luxo - Fio Tinto - Voil - Voil Flame - Linho 
            // Sarja - Percal - Cetim - Oxford - Oxford Xadrez - Blackout - Renda - Suede - Nobuck - Corano - Linen Look - Brugges
            {
                brandIds: [2000003,2000005,2000006,2000007,2000008,2000012,2000013,2000014,2000018,2000017,2000011,2000009,2000010,2000015,2000026,2000022,2000023,2000024,2000025,2000021,2000019,2000020,2000016],
                filters: [
                    {
                        id: 19,
                        name: 'Cor Predominante'
                    }, 
                    {
                        id: 20,
                        name: 'Tom Predominante'
                    }, 
                    {
                        id: 21,
                        name: 'Desenho'
                    },
                    {
                        id: 23,
                        name: 'Orientação da estampa'
                    },
                    {
                        id: 24,
                        name: 'Sugestão de aplicação'
                    }, 
                    {
                        id: 25,
                        name: 'Largura do tecido'
                    }, 
                    {
                        id: 26,
                        name: 'Pré-Lavagem'
                    }
                ]
            },

            // Jacquard Estampado - Party Decor - Gorgurinho - Tricoline - Dacqua - Summer - Piede Poule - Viscose - Wall Linea - Neoplex
            {
                brandIds: [2000004,2000001,2000002,2000032,2000028,2000029,2000030,2000033,2000027,2000031,2000034],
                filters: [
                    {
                        id: 19,
                        name: 'Cor Predominante'
                    }, 
                    {
                        id: 20,
                        name: 'Tom Predominante'
                    },
                    {
                        id: 22,
                        name: 'Estampa'
                    }, 
                    {
                        id: 23,
                        name: 'Orientação da estampa'
                    }, 
                    {
                        id: 24,
                        name: 'Sugestão de aplicação'
                    }, 
                    {
                        id: 25,
                        name: 'Largura do tecido'
                    }, 
                    {
                        id: 26,
                        name: 'Pré-Lavagem'
                    }
                ]
            },
        ]
    }
});