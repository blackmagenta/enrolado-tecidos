define(function(){
    return {
        themes: [
            {
                name: 'Circo',
                link: '/temas/circo/'
            },
            {
                name: 'Minions',
                link: '/temas/minions/'
            },
            {
                name: 'Frozen',
                link: '/temas/frozen/'
            },
            {
                name: 'Mickey e Minnie',
                link: '/temas/mickey-e-minnie/'
            },
            {
                name: 'Fazendinha',
                link: '/temas/fazendinha/'
            },
            {
                name: 'Boteco',
                link: '/temas/boteco/'
            },
            {
                name: 'Festa de Menino',
                link: '/temas/festa-de-menino/'
            },
            {
                name: 'Festa de Menina',
                link: '/temas/festa-de-menina/'
            },
            {
                name: 'Peppa Pig',
                link: '/temas/peppa-pig/'
            },
            {
                name: 'Princesa Sofia',
                link: '/temas/princesa-sofia/'
            },
            {
                name: 'Bodas',
                link: '/temas/bodas/'
            },
            {
                name: 'Animais',
                link: '/temas/animais/'
            },
            {
                name: 'Unicórnio',
                link: '/temas/unicornio/'
            },
            {
                name: 'Ladybug',
                link: '/temas/ladybug/'
            },
            {
                name: 'Patrulha Canina',
                link: '/temas/patrulha-canina/'
            },
            {
                name: 'Bailarina',
                link: '/temas/bailarina/'
            },
            {
                name: 'Festa de 15 Anos',
                link: '/temas/festa-de-15-anos/'
            },
            {
                name: 'Fundo do Mar',
                link: '/temas/fundo-do-mar/'
            },
            {
                name: 'Chá Revelação',
                link: '/temas/cha-revelacao/'
            },
            {
                name: 'Copa do Mundo',
                link: '/temas/copa-do-mundo/'
            },
            {
                name: 'Festa Junina',
                link: '/temas/festa-junina/'
            },
        ]
    }
});