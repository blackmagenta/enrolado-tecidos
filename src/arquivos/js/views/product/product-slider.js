define(function () {
	// product/product-slider.dust
	(function(){dust.register("product/product-slider",body_0);function body_0(chk,ctx){return chk.write("<div class=\"flexslider flex-catalog\"><ul class=\"slides\">").section(ctx.get("products"),ctx,{"block":body_1},null).write("</ul></div>");}function body_1(chk,ctx){return chk.write("<li>").partial("product/product-thumbnail",ctx,null).write("</li>");}return body_0;})();
	return "product/product-slider";
});