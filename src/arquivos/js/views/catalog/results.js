define(function () {
	// catalog/results.dust
	(function(){dust.register("catalog/results",body_0);function body_0(chk,ctx){return chk.partial("catalog/ordering",ctx,null).write("<div class=\"row\">").section(ctx.get("products"),ctx,{"block":body_1},null).write("</div>").partial("catalog/pagination",ctx,null);}function body_1(chk,ctx){return chk.write("<div class=\"col-6 col-md-4 col-xl-3\">").partial("product/product-thumbnail",ctx,null).write("</div>");}return body_0;})();
	return "catalog/results";
});