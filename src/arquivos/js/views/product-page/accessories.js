define(function () {
	// product-page/accessories.dust
	(function(){dust.register("product-page/accessories",body_0);function body_0(chk,ctx){return chk.write("<h3 class=\"homepage__heading\">Sugerimos para você:</h3><p>Pensando em uma solução perfeita para sua compra, sugerimos alguns produtos que combinam perfeitamente com a sua escolha.</p><div class=\"row justify-content-center\"><div class=\"col-xl-10\">").section(ctx.get("products"),ctx,{"block":body_1},null).write("</div></div>");}function body_1(chk,ctx){return chk.partial("product/product-slider",ctx,null);}return body_0;})();
	return "product-page/accessories";
});