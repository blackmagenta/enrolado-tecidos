define(function () {
	// product-page/payment.dust
	(function(){dust.register("product-page/payment",body_0);function body_0(chk,ctx){return chk.write("<div class=\"row\">").exists(ctx.get("wholeSale"),ctx,{"block":body_1},null).write("<div class=\"payment-methods col-6 col-lg-12\"><a href=\"#payment-methods\" data-toggle=\"modal\" class=\"payment-methods__btn\">Formas de pagamento</a></div></div>");}function body_1(chk,ctx){return chk.write("<div class=\"pricing-table col-6 col-lg-12\"><a href=\"#pricing-table\" data-toggle=\"modal\" class=\"pricing-table__btn\">Tabela de preços</a></div>");}return body_0;})();
	return "product-page/payment";
});