define(function () {
	// product-page/designs.dust
	(function(){dust.register("product-page/designs",body_0);function body_0(chk,ctx){return chk.write("<h4 class=\"other-designs__heading\">Outros desenhos disponíves desta cor:</h4><ul class=\"other-designs__list\">").section(ctx.get("otherDesigns"),ctx,{"block":body_1},null).write("</ul>");}function body_1(chk,ctx){return chk.write("<li class=\"other-designs__item\"><a href=\"").reference(ctx.get("url"),ctx,"h").write("\" title=\"").reference(ctx.get("name"),ctx,"h").write("\" class=\"other-designs__design\" style=\"background-image: url(").reference(ctx.get("imageUrl"),ctx,"h").write(");\"></a></li>");}return body_0;})();
	return "product-page/designs";
});