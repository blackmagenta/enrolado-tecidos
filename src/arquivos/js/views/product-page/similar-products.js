define(function () {
	// product-page/similar-products.dust
	(function(){dust.register("product-page/similar-products",body_0);function body_0(chk,ctx){return chk.write("<h3 class=\"homepage__heading\">Produtos Similares:</h3><div class=\"row justify-content-center\"><div class=\"col-xl-10\">").partial("product/product-slider",ctx,null).write("</div></div>");}return body_0;})();
	return "product-page/similar-products";
});