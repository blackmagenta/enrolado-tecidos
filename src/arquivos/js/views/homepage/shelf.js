define(function () {
	// homepage/shelf.dust
	(function(){dust.register("homepage/shelf",body_0);function body_0(chk,ctx){return chk.write("<div class=\"row\">").section(ctx.get("products"),ctx,{"block":body_1},null).write("</div>");}function body_1(chk,ctx){return chk.write("<div class=\"col-lg-3\">").partial("product/product-thumbnail",ctx,null).write("</div>");}return body_0;})();
	return "homepage/shelf";
});