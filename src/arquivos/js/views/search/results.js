define(function () {
	// search/results.dust
	(function(){dust.register("search/results",body_0);function body_0(chk,ctx){return chk.section(ctx.get("results"),ctx,{"block":body_1},null);}function body_1(chk,ctx){return chk.write("<a href=\"").reference(ctx.get("link"),ctx,"h").write("\" class=\"search__result dropdown-item d-flex align-items-center clearfix\"><img src=\"").reference(ctx.get("imageUrl"),ctx,"h").write("\" alt=\"").reference(ctx.get("name"),ctx,"h").write("\" width=\"30\" height=\"30\" class=\"result__image img-fluid\" /><span class=\"result__name\">").reference(ctx.get("nameHighlighted"),ctx,"h").write("</span></a>");}return body_0;})();
	return "search/results";
});