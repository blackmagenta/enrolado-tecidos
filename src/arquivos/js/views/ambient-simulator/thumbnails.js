define(function () {
	// ambient-simulator/thumbnails.dust
	(function(){dust.register("ambient-simulator/thumbnails",body_0);function body_0(chk,ctx){return chk.write("<div class=\"product__other-designs\"><ul class=\"other-designs__list\">").section(ctx.get("simulations"),ctx,{"block":body_1},null).write("</ul></div>");}function body_1(chk,ctx){return chk.write("<li class=\"other-designs__item\"><a href=\"").reference(ctx.get("id"),ctx,"h").write("\" title=\"").reference(ctx.get("name"),ctx,"h").write("\" class=\"other-designs__design\" style=\"background-image: url(").reference(ctx.get("thumbnail"),ctx,"h").write(");\"></a></li>");}return body_0;})();
	return "ambient-simulator/thumbnails";
});