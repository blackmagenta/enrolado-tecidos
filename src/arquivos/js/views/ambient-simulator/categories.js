define(function () {
	// ambient-simulator/categories.dust
	(function(){dust.register("ambient-simulator/categories",body_0);function body_0(chk,ctx){return chk.write("<option name=\"categoria\" value=\"\" selected=\"true\">Escolha a categoria</option>").section(ctx.get("categories"),ctx,{"block":body_1},null);}function body_1(chk,ctx){return chk.write("<option name=\"categoria\" value=\"").reference(ctx.get("id"),ctx,"h").write("\">").reference(ctx.get("nome"),ctx,"h").write("</option>");}return body_0;})();
	return "ambient-simulator/categories";
});