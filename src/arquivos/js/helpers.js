define(function(){
    return {
        loading: function(bool){
            var page = $('body'),
                toggleClass = 'body--loading';
    
            if (bool == true) {
                page.addClass(toggleClass);
            } else {
                page.removeClass(toggleClass);
            };
        },
    
        alert: function(context, type, message){
            var context = $(context).find('.alert');
    
            context.removeClass('alert-danger alert-success')
                .addClass('alert-'+type)
                .html(message)
                .delay(350)
                .slideDown(350, function(){
                    var thisAlert = $(this);

                    if (!context.parent().hasClass('shipping__form')) {
                        thisAlert.delay(1000)
                            .slideUp(350, function(){
                                thisAlert.empty();
                            });
                    };
                });
        },
    
        formatPrice: function(price){
            return defaultStoreCurrency + (price / 100).toFixed(2).replace('.', ',');
        },

        convertToSlug: function(val, replaceBy) {
            replaceBy = replaceBy || '-';
            var mapaAcentosHex 	= {
                a : /[\xE0-\xE6]/g,
                A : /[\xC0-\xC6]/g,
                e : /[\xE8-\xEB]/g,
                E : /[\xC8-\xCB]/g,
                i : /[\xEC-\xEF]/g,
                I : /[\xCC-\xCF]/g,
                o : /[\xF2-\xF6]/g,
                O : /[\xD2-\xD6]/g,
                u : /[\xF9-\xFC]/g,
                U : /[\xD9-\xDC]/g,
                c : /\xE7/g,
                C : /\xC7/g,
                n : /\xF1/g,
                N : /\xD1/g,
            };
            
            for ( var letra in mapaAcentosHex ) {
                var expressaoRegular = mapaAcentosHex[letra];
                val = val.replace( expressaoRegular, letra );
            };
            
            val = val.toLowerCase();
            val = val.replace(/[^a-z0-9\-]/g, " ");
            
            val = val.replace(/ {2,}/g, " ");
              
            val = val.trim();    
            val = val.replace(/\s/g, replaceBy);
            
            return val;
        },

        getUrlParameter: function(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.href);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        },
    
        searchProduct: function(term){
            return $.ajax({
                crossDomain: true,
                url: '/api/catalog_system/pub/products/search/' + term,
                method: 'GET',
                headers: {
                    "Accept": "application/json",
                    'Content-Type': 'application/json'
                }
            });
        },

        searchCategory: function(categoryId){
            return $.ajax({
                crossDomain: true,
                url: "/api/catalog_system/pvt/category/" + categoryId,
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "X-VTEX-API-AppKey": "vtexappkey-enroladotecidos-KVZFFN",
                    "X-VTEX-API-AppToken": "PJYUHXZOWTMZGTPAHTZOURAOONKQSUZGMNKDFPSYVFQSDODPVHQRKGKYIKFTMIWCTFMSIMVSBASOBIKAJVVKDIWFCMVVKZLMJNZYCLHJMYQJOVWZZFJRNMZKJGPVQAIB"
                }
            });
        },

        searchBrands: function(){
            return $.ajax({
                crossDomain: true,
                ontentType: "application/json",
                url: '/api/catalog_system/pvt/brand/list',
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "X-VTEX-API-AppKey": "vtexappkey-enroladotecidos-KVZFFN",
                    "X-VTEX-API-AppToken": "PJYUHXZOWTMZGTPAHTZOURAOONKQSUZGMNKDFPSYVFQSDODPVHQRKGKYIKFTMIWCTFMSIMVSBASOBIKAJVVKDIWFCMVVKZLMJNZYCLHJMYQJOVWZZFJRNMZKJGPVQAIB"
                }
            });
        },

        searchBrand: function(brand){
            return $.ajax({
                crossDomain: true,
                url: "/api/catalog_system/pvt/brand/" + brand,
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "X-VTEX-API-AppKey": "vtexappkey-enroladotecidos-KVZFFN",
                    "X-VTEX-API-AppToken": "PJYUHXZOWTMZGTPAHTZOURAOONKQSUZGMNKDFPSYVFQSDODPVHQRKGKYIKFTMIWCTFMSIMVSBASOBIKAJVVKDIWFCMVVKZLMJNZYCLHJMYQJOVWZZFJRNMZKJGPVQAIB"
                }
            });
        },
    
        simulateProductPrice: function(sku, quantity, seller){
            return $.ajax({
                contentType: 'application/json; charset=UTF-8',
                dataType: 'json',
                type: 'POST',
                url: '/api/checkout/pub/orderForms/simulation?sc=1',
                data: JSON.stringify({
                    'items': [{
                        'id': sku,
                        'quantity': quantity,
                        'seller': seller
                    }],
                    'postalCode': null,
                    'country': localeInfo.countryCode
                })
            });
        },

        simulateProductShipping: function(items, postalCode, country){
            return vtexjs.checkout.simulateShipping(items, postalCode, country);
        }
    };
});