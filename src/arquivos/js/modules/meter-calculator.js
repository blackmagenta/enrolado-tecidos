define(['../mocks/table-sizes', '../views/meter-calculator/meter-calculator'], function(tables){
    dust.render('meter-calculator/meter-calculator', { tables: tables }, function(err, out){
        $('.meter-calculator .page-container').html(out);

        $('.meter-calculator__option').click(function(){
            var target = $(this).data('target');

            $('.meter-calculator__option').removeClass('meter-calculator__option--active');
            $(this).addClass('meter-calculator__option--active');

            $('.meter-calculator__content-container').addClass('meter-calculator__content-container--active');

            $('.meter-calculator__content').removeClass('meter-calculator__content--active');
            $('.meter-calculator__content--' + target).addClass('meter-calculator__content--active');
        });

        var formatResult = function(result) {
            return Math.ceil(result + ((result / 100) * 5));
        };

        $('.meter-calculator-step-3__btn').click(function(){
            var height = Number($('[name="altura"]').val()),
                width = Number($('[name="largura"]').val()),
                horizontalResult = formatResult(width),
                verticalResult1 = formatResult( (height * 1.1) * width ),
                verticalResult2 = Math.ceil((height * 1.2) * (Math.floor(1 + (width/2.8))))
            
            $('.meter-calculator-result__result--horizontal').fadeOut();
            $('.meter-calculator-result__result--vertical').fadeOut();

            if (height <= 2.8) {
                // show both horizontal and vertical designs (2,8 horizontal, 1,4 and 2,8 vertical)

                $('.meter-calculator-result__result--horizontal').fadeIn();
                $('.meter-calculator-result__result--vertical').fadeIn();

                $('.meter-calculator-result__number--horizontal span').html(horizontalResult);
                $('.meter-calculator-result__number--vertical-1-4 span').html(verticalResult1);
                $('.meter-calculator-result__number--vertical-2-8 span').html(verticalResult2);
            } else {
                // show only vertical designs (1,4 and 2,8)

                $('.meter-calculator-result__result--vertical').fadeIn();

                $('.meter-calculator-result__number--vertical-1-4 span').html(verticalResult1);
                $('.meter-calculator-result__number--vertical-2-8 span').html(verticalResult2);
            }

            $('.meter-calculator__content--wall .meter-calculator__result').addClass('meter-calculator__result--active');
        });

        $('.tipo-toalha').change(function(){
            var selectedTable = $(this).val();

            var selectedTableIndex = tables.map(function(table){
                return table.value;
            }).indexOf(selectedTable);

            var tableSizes = ['<option name="lugares" value="">Número de lugares</option>'];

            tables[selectedTableIndex].sizes.forEach(function(size){
                tableSizes.push('<option name="lugares" value="' + size.meters + '">' + size.seats + ' Lugares</option>');
            });

            $('.lugares').html(tableSizes);

            $('.meter-calculator__step--3').addClass('meter-calculator__step-3--active');
        });

        $('.lugares').change(function(){
            var result = $(this).val();
            $('.meter-calculator-result__number span').html(result);
            $('.meter-calculator__result').addClass('meter-calculator__result--active');
        });

        $('.meter-calculator-step-3__btn--redo').click(function(){
            $('.tipo-toalha').val('');
            $('.lugares').val('');
            $('.meter-calculator__result').removeClass('meter-calculator__result--active');
            $('.meter-calculator__step--3').removeClass('meter-calculator__step-3--active');

            $('body, html').animate({scrollTop: 0}, 400);
        });
    });
});