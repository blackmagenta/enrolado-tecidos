define(['helpers', '../vendor/cidade-estados'], function(helpers){      
    $('[name="newsletter"]').change(function(){
        ($(this).prop('checked')) ? $(this).val(true) : $(this).val(false);
    });

    if ($('#estado').length && $('#cidade').length) {
        new dgCidadesEstados({
            cidade: document.getElementById('cidade'),
            estado: document.getElementById('estado')
        });
    };

    $('.contact-form').submit(function(e){
        e.preventDefault();

        $(this).addClass('contact-form--sending');

        var result = {};
        $.each($(this).serializeArray(), function() {
            result[this.name] = this.value;
        });

        $.ajax({
            crossDomain: true,
            url: "/api/dataentities/CO/documents/",
            type: "POST",
            method: "POST",
            data: JSON.stringify(result),
            processData: false,
            headers: {
                "Content-Type": "application/json",
                "X-VTEX-API-AppKey": "vtexappkey-enroladotecidos-KVZFFN",
                "X-VTEX-API-AppToken": "PJYUHXZOWTMZGTPAHTZOURAOONKQSUZGMNKDFPSYVFQSDODPVHQRKGKYIKFTMIWCTFMSIMVSBASOBIKAJVVKDIWFCMVVKZLMJNZYCLHJMYQJOVWZZFJRNMZKJGPVQAIB"
            }
        }).then(function(results, status){
            if (status === "success") {
                if ($('body').hasClass('specialist')) {
                    $('.specialist-confirmation').modal('show');
                } else {
                    $('.contact-confirmation').modal('show');
                }
            } else {
                $('.contact-error').modal('show');
            }
        }).fail(function(results, status){
            console.log(results, status);
        });
    });


    // newsletter

    $('.newsletter').submit(function(e){
        e.preventDefault();

        $.ajax({
            crossDomain: true, 
            url: "https://api.elasticemail.com/v2/contact/add?sendActivation=false&" + $(this).serialize(),
            type: "POST",
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(function(a, b, status){
            if (status.status === 200) {
                $('#newsletter-confirmation').modal('show');
            };
        }).fail(function(results, status){
            console.log(results, status);
        });
    });
});