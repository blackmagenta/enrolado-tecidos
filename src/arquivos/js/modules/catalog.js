define([
    'helpers',
    '../mocks/navigation-tree',
    '../mocks/filter-sets',
    'flexslider',
    '../views/catalog/results', 
    '../views/product/product-thumbnail', 
    '../views/catalog/ordering', 
    '../views/catalog/pagination',
    '../views/catalog/active-filters', 
    '../views/catalog/filters',
    '../views/catalog/brands'
], function(helpers, navigationTree, filterSets){
    var settings = {
            orderOptions: [
                {
                    value: 'OrderByReleaseDateDESC',
                    text: false,
                    activeOrder: true
                },
                {
                    value: 'OrderByTopSaleDESC',
                    text:  'Mais vendidos'
                },
                {
                    value: 'OrderByNameASC',
                    text:  'Ordem alfabética'
                },
                {
                    value: 'OrderByPriceASC',
                    text:  'Menor preço'
                },
                {
                    value: 'OrderByPriceDESC',
                    text:  'Maior preço'
                }
            ],
            productsPerPage: 30
        },
        canonicalUrl = $('link[rel="canonical"]').attr('href'),
        parameterizedUrl = canonicalUrl,
        queryParameters = {
            category: {
                ids: '',
                name: '',
            },
            brand: {
                id: 0,
                name: ''
            },
            cluster: {
                id: ''
            },
            filters: {
                query: '',
                url: ''
            },
            paging: {
                from: 0,
                to: (settings.productsPerPage - 1),
                currentPage: 1
            },
            query: ''
        },
        data = {
            orderOptions: settings.orderOptions
        };


    // check cluster parameters

    var checkClusterParameters = function(clusterId){        
        queryParameters.cluster.id = clusterId;
    };
    

    // check ordering parameters

    var checkOrderingParameters = function(order){
        var orderIndex = settings.orderOptions.map(function(orderOption) {
            return orderOption.value;
        }).indexOf(order);
        
        settings.orderOptions.forEach(function(option){
            option.activeOrder = false;
        });
        
        settings.orderOptions[orderIndex].activeOrder = true;
    };


    // check paging parameter

    var checkPageParamenter = function(page){
        if (page > 1) {
            queryParameters.paging.currentPage = page;
            queryParameters.paging.from = ((page * settings.productsPerPage) - settings.productsPerPage);
            queryParameters.paging.to = (queryParameters.paging.from + settings.productsPerPage) - 1;
        } else {
            queryParameters.paging.currentPage = 1;
            queryParameters.paging.from = 0;
            queryParameters.paging.to = (settings.productsPerPage - 1);
        };
    };


    // check filter parameters

    var checkFilterParameters = function(filterParams){
        var filtersArray = filterParams.split(';'),
            filters = [];

        filtersArray.forEach(function(filterParam, index){
            if (index + 1 != filtersArray.length) {
                var filterParamId = filterParam.split('~')[0],
                    filterParamValue = filterParam.split('~')[1];

                if (filterParamId === 'Price') {
                    var priceFrom = filterParamValue.split('-')[0];
                    var priceTo = filterParamValue.split('-')[1];

                    var filter = '&fq=P:[' + priceFrom + ' TO ' + priceTo + ']';
                } else {
                    var filter = '&fq=specificationFilter_' + filterParamId + ':' + filterParamValue;
                }

                filters.push(filter);
            };
        });

        queryParameters.filters.query = filters.join('');
        queryParameters.filters.url = filterParams;
    };


    var updateQuery = function(){
        helpers.loading(true);

        if (vtxctx.searchTerm != '') {
            queryParameters.query = '?ft=' + vtxctx.searchTerm;
        } else if (queryParameters.cluster.id != '') {
            queryParameters.query = '?fq=productClusterIds:' + queryParameters.cluster.id;
        } else {
            if (queryParameters.category.ids && queryParameters.brand.id != '') {
                queryParameters.query = '?fq=C:' + queryParameters.category.ids + '&fq=B:' + queryParameters.brand.id;
            } else if (queryParameters.category.ids && !queryParameters.brand.id != 0) {
                queryParameters.query = '?fq=C:' + queryParameters.category.ids;
            } else if (!queryParameters.category.ids && queryParameters.brand.id != 0) {
                queryParameters.query = '?fq=B:' + queryParameters.brand.id;
            };
        };

        var activeOrderIndex = settings.orderOptions.map(function(option){
            return option.activeOrder
        }).indexOf(true);

        queryParameters.query += '&O=' + settings.orderOptions[activeOrderIndex].value;
        
        if (queryParameters.filters.url != '') {
            queryParameters.query += queryParameters.filters.query;
            parameterizedUrl += '&F=' + queryParameters.filters.url;
        };
        
        queryParameters.query += '&_from=' + queryParameters.paging.from + '&_to=' + queryParameters.paging.to;

        if (queryParameters.cluster.id != '') {
            parameterizedUrl = canonicalUrl + 'busca/?H=' + queryParameters.cluster.id + '&O=' + settings.orderOptions[activeOrderIndex].value;
        } else {
            parameterizedUrl = canonicalUrl + '?O=' + settings.orderOptions[activeOrderIndex].value;
        };
        
        if (queryParameters.filters.url != '') {
            parameterizedUrl += '&F=' + queryParameters.filters.url;
        };
        
        parameterizedUrl += '&P=' + queryParameters.paging.currentPage;
        history.pushState(null, '', parameterizedUrl);
        
        searchProducts(queryParameters.query);

        if (vtxctx.searchTerm == '') {
            if (queryParameters.category.ids.indexOf('/') != -1) {
                var categoryId = queryParameters.category.ids.split('/'),
                    categoryId = categoryId[categoryId.length - 1];
            } else {
                var categoryId = queryParameters.category.ids;
            };

            brands();
            activeFilters();
        } else {
            $('.filters').hide();
        };

        if (queryParameters.cluster.id != '') {
            $('.filters').hide();
        }
    };

    // search for products

    var searchProducts = function(parameters) {
        helpers.searchProduct(parameters)
        .then(function(results, status, response){
            if (queryParameters.cluster.id != '') {
                $('.catalog-banner__heading').html('<h2 class="titulo-sessao">' + results[0].productClusters[queryParameters.cluster.id] + '</h2');
            };

            helpers.loading(false);

            var totalProducts = response.getResponseHeader('resources').split('/')[1];

            paging(totalProducts);
        
            results.forEach(function(result){
                var resultItem = result.items[0],
                    resultCommertialOffer = resultItem.sellers[0].commertialOffer;
                
                result.sku = resultItem.itemId;
                result.imageId = resultItem.images[0].imageId;
                result.imageUrl = resultItem.images[0].imageUrl.replace(result.imageId, result.imageId + '-300-300');
                result.seller = resultItem.sellers[0].sellerId;
                result.availableQuantity = resultCommertialOffer.AvailableQuantity;
                result.isInStock = (result.availableQuantity > 0);
                result.listPrice = resultCommertialOffer.ListPrice;
                result.price = resultCommertialOffer.Price;
                result.listPriceFormatted = defaultStoreCurrency + (resultCommertialOffer.ListPrice.toFixed(2).replace('.', ','));
                result.priceFormatted = defaultStoreCurrency + (resultCommertialOffer.Price.toFixed(2).replace('.', ','));
                result.flag = resultCommertialOffer.DiscountHighLight;

                if (result.flag.length) {
                    result.flag = result.flag[0]['<Name>k__BackingField'];
                };

                if (resultItem.images[1]) {
                    result.secondImageUrl = resultItem.images[1].imageUrl.replace(resultItem.images[1].imageId, resultItem.images[1].imageId + '-300-300');
                };

                if (result.listPrice > result.price) {
                    result.hasBestPrice = true;
                }

                if (result['Tipo de Seletor']) {
                    result.selector = result['Tipo de Seletor'][0];
                };

                if (result.selector === 'Seletor de Metro') {
                    result.meterSelector = true;
                    result.priceFormatted += '/m';
                } else if (result.selector === 'SKU (exemplo tamanho e cor)') {
                    result.skuSelector = true;
                } else {
                    result.quantitySelector = true;
                };
            });

            data.totalProducts = totalProducts;
            data.products = results;

            dust.render('catalog/results', data, function(err, out){
                var topColors = data.products.slice();

                var currentIndex = topColors.length, 
                    temporaryValue, 
                    randomIndex;

                while (0 !== currentIndex) {
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;
                
                    temporaryValue = topColors[currentIndex];
                    topColors[currentIndex] = topColors[randomIndex];
                    topColors[randomIndex] = temporaryValue;
                };

                topColors.forEach(function(product){
                    topColors.push('<li class="cores__item cores__item--' + product.sku + '" data-name="' + product.productName + '" style="background: url(' + product.imageUrl + ') center center /150% no-repeat;"><a href="' + product.link + '" class="cores__link" title="' + product.productName + '">' + product.productName + '</a></li>');
                });

                $('.cores__list--catalog').html(topColors);

                $('.flex-colors--catalog').flexslider({
                    slideShow: false,
                    animation: "slide",
                    animationLoop: false,
                    controlNav: false,
                    itemWidth: 50,
                    itemMargin: 10,
                    minItems: 6,
                    maxItems: 25
                });


                $('.results').html(out);

                (function () {
                    var yvs = document.createElement("script");
                    yvs.type = "text/javascript";
                    yvs.async = true;
                    yvs.id = "_yvsrc";
                    yvs.src = "//service.yourviews.com.br/script/4790dc80-47b3-466f-b394-82d3feb57bc6/yvapi.js";
                    var yvs_script = document.getElementsByTagName("script")[0];
                    yvs_script.parentNode.insertBefore(yvs, yvs_script);
                })();

                $('.product-thumbnail').each(function(index){
                    var thumbnail = $(this),
                        thumbnailSku = $(this).data('product-sku'),
                        thumbnailImg = thumbnail.find('.product-thumbnail__image'),
                        wholesalePriceContainer = thumbnail.find('.wholesale-price__price');

                    if (data.products[index].secondImageUrl) {
                        thumbnailImg
                            .addClass('product-thumbnail__image--multiple')
                            .append('<img src="' + data.products[index].secondImageUrl + '" alt="" />');
                    };

                    helpers.simulateProductPrice(thumbnailSku, 50, 1).then(function(product){
                        var wholesalePrice,
                            sellingPrice = product.items[0].sellingPrice,
                            price = product.items[0].price,
                            measurementUnit = product.items[0].measurementUnit;
            
                        if (sellingPrice < price) {
                            wholesalePriceContainer.html(helpers.formatPrice(sellingPrice) + '/' + measurementUnit);
                        } else {
                            wholesalePriceContainer.parent().hide();
                            thumbnail.find('.product-thumbnail__price').removeClass('product-thumbnail__price--wholesale');
                        };
                    });
                });

                $('.product-thumbnail__meter-selector').change(function(){
                    $(this).next().data('quantity', $(this).val());
                });

                $('body, html').animate({scrollTop: 0}, 650);

                if (!$('.filters').find('.filters__close').length) {
                    $('.filters').prepend('<a class="btn btn--outline-gray filters__close" href="#">Fechar</a>');
                };

                $('.filters__close').click(function(e){
                    e.preventDefault();
                    $('.filters').removeClass('filters--visible');
                });

                if (vtxctx.searchTerm != '') {
                    $('.filters__toggler').hide();
                };
                
                $('.filters__toggler').click(function(e){
                    e.preventDefault();
                    
                    if ($('.filters').hasClass('filters--visible')) {
                        $('.filters').removeClass('filters--visible');
                    } else {
                        $('.filters').addClass('filters--visible');
                    }
                });

                $('.order__option').click(function(e){
                    e.preventDefault();
                    
                    var clickedOrder = $(this).data('order');

                    checkOrderingParameters(clickedOrder);
                    checkPageParamenter(1);
                    updateQuery();
                });

                $('.page-link').click(function(e){
                    e.preventDefault();
                    
                    var clickedPage = Number($(this).data('page'));

                    checkPageParamenter(clickedPage);
                    updateQuery();
                });
            });
        });
    };


    // paging

    var paging = function(totalProducts){
        var totalPages = Math.ceil(totalProducts / settings.productsPerPage),
            pages = [],
            previousPage = false,
            nextPage = false;
        
        for (var i = 0; i < totalPages; i++) {
            var page = {
                pageNumber: (i + 1),
                isCurrentPage: false
            };

            if (i + 1 === queryParameters.paging.currentPage){
                page.isCurrentPage = true;
            };

            pages.push(page);
        };

        if (queryParameters.paging.currentPage != 1) {
            previousPage = {
                pageNumber: (queryParameters.paging.currentPage - 1)
            };
        };

        if (queryParameters.paging.currentPage != totalPages && totalProducts > 16) {
            nextPage = {
                pageNumber: (queryParameters.paging.currentPage + 1)
            };
        };

        if (queryParameters.paging.currentPage > totalPages || queryParameters.paging.currentPage < 1) {
            queryParameters.paging.currentPage = 1;
        };

        if (pages.length > 6 && queryParameters.paging.currentPage < 3) {
            var selectedPages = pages.slice(0,5);
        } else if (pages.length > 6 && queryParameters.paging.currentPage > 2 && queryParameters.paging.currentPage <= totalPages - 2) {
            var selectedPages = pages.slice((queryParameters.paging.currentPage - 3), (queryParameters.paging.currentPage + 2));
        } else if (pages.length > 6 && queryParameters.paging.currentPage > totalPages - 2) {
            var selectedPages = pages.slice((queryParameters.paging.currentPage - 5), totalPages);
        } else {
            var selectedPages = pages;
        };

        data.pagination = {
            pages: selectedPages,
            currentPage: queryParameters.paging.currentPage,
            previousPage: previousPage,
            nextPage: nextPage
        };
    };


    // brands

    var brands = function(){
        var catalogDepth = canonicalUrl.split('/').splice(3, Number.MAX_VALUE);

        var departmentIndex = navigationTree.menu.map(function(department){
            return helpers.convertToSlug(department.name);
        }).indexOf(catalogDepth[0]);

        var renderBrands = function(brands, hasSelectedBrand){
            if (brands.length) {
                brands.sort(function(a, b) {
                    var textA = a.name.toUpperCase();
                    var textB = b.name.toUpperCase();
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });
        
                dust.render('catalog/brands', { brands: brands, hasSelectedBrand: hasSelectedBrand }, function(err, out){
                    $('.filters__brands').html(out);
                });
            };
        };

        if (catalogDepth.length === 1 && departmentIndex != -1) {
            var brands = [];

            navigationTree.menu[departmentIndex].children.forEach(function(category){
                category.children.forEach(function(brand){
                    if (brand.link.split('/').length === 4) {
                        var brandIndex = brands.map(function(categoryBrand){
                            return categoryBrand.slug;
                        }).indexOf(brand.slug);
    
                        if (brandIndex == -1) {
                            var brandLink = brand.link.split('/');
                            brand.url = brandLink[1] + '/' + brandLink[3];
    
                            brands.push(brand);
                        };
                    };
                });
            });

            renderBrands(brands, false);
        } else if (catalogDepth.length === 1 && departmentIndex === -1) {
            var brands = [];

            helpers.searchBrands().then(function(results){
                var currentBrandIndex = results.map(function(brand) {
                    return helpers.convertToSlug(brand.name);
                }).indexOf(String(catalogDepth[catalogDepth.length -1]));
    
                results.forEach(function(brand){
                    brand.slug = helpers.convertToSlug(brand.name);
    
                    if (currentBrandIndex != -1) {
                        results[currentBrandIndex].isSelectedBrand = true;
                        brand.url = canonicalUrl.replace(catalogDepth[catalogDepth.length - 1], '') + brand.slug;
                    } else {
                        brand.url = canonicalUrl + '/' + brand.slug;
                    };
    
                    brands.push(brand);
                });

                renderBrands(brands, true);
            });
        } else if (catalogDepth.length != 1 && catalogDepth.length >= 2) {
            var brands = [],
                hasSelectedBrand = false,
                categoryIndex = navigationTree.menu[departmentIndex].children.map(function(category){
                    var categoryLink = category.link.split('/');
                    return categoryLink[categoryLink.length - 1];
                }).indexOf(catalogDepth[1]);

            if (categoryIndex != -1) {
                navigationTree.menu[departmentIndex].children[categoryIndex].children.forEach(function(brand){
                    brand.url = brand.link;

                    var links = brand.link.split('/');

                    if (helpers.convertToSlug(links[links.length -1]) === catalogDepth[catalogDepth.length -1]) {
                        brand.isSelectedBrand = true;
                        hasSelectedBrand = true;
                    };

                    brands.push(brand);
                });
            } else {
                navigationTree.menu[departmentIndex].children.forEach(function(category){
                    category.children.forEach(function(brand){
                        if (helpers.convertToSlug(brand.link.split('/')[brand.link.split('/').length -1]) === catalogDepth[catalogDepth.length -1]) {
                            brand.isSelectedBrand = true;
                            hasSelectedBrand = true;
                        };

                        if (brand.link.split('/').length === 4) {
                            var brandIndex = brands.map(function(categoryBrand){
                                return categoryBrand.slug;
                            }).indexOf(brand.slug);
        
                            if (brandIndex == -1) {
                                var brandLink = brand.link.split('/');
                                brand.url = brandLink[1] + '/' + brandLink[3];
        
                                brands.push(brand);
                            };
                        };
                    });
                });
            };

            renderBrands(brands, hasSelectedBrand);
        };
    };
    

    // active filters

    var activeFilters = function(){
        if (queryParameters.filters.url){
            var filtersParameters = queryParameters.filters.url.split(';'),
                activeFilters = [];

            filtersParameters.forEach(function(filterParameter, index){
                if (index + 1 != filtersParameters.length) {
                    var filterId = filterParameter.split('~')[0],
                        filterValue = filterParameter.split('~')[1],
                        filterLabel = filterParameter.split('~')[1];

                    if (filterId === 'Price') {
                        filterLabel = 'R$' + filterValue.split('-')[0] + ' - R$' + filterValue.split('-')[1];
                    };
                        
                    var activeFilter = {
                        id: filterId,
                        value: filterValue,
                        label: filterLabel
                    };

                    activeFilters.push(activeFilter);
                };
            });

            var data = {
                activeFilters: activeFilters
            };

            dust.render('catalog/active-filters', data, function(err, out){
                $('.filters__active-filters').html(out).show();

                $('.filters__clear').click(function(e){
                    e.preventDefault();

                    $('.filter__checkbox').prop('checked', false);

                    if (helpers.getUrlParameter('F')) {
                        checkFilterParameters('');
                    };
                    
                    updateQuery();
                });

                $('.active-filters__link').click(function(e){
                    e.preventDefault();

                    var filterId = $(this).data('filter-id'),
                        filterValue = $(this).data('filter-value');

                    $('.filter__checkbox[name="'+ filterValue +'"]').prop('checked', false);

                    if ((parameterizedUrl.match(/;/g) || []).length > 1) {
                        history.pushState(null, '', parameterizedUrl.replace(filterId + '~' + filterValue + ';', ''));
                    } else {
                        history.pushState(null, '', parameterizedUrl.replace('&F=' + filterId + '~' + filterValue + ';', ''));
                    };

                    checkFilterParameters(helpers.getUrlParameter('F'));
                    checkPageParamenter(1);
                    updateQuery();
                });
            });
        } else {
            $('.filters__active-filters').empty().hide();
        };
    };


    // filters

    var filters = function(brandId){
        var brandIndex = -1,
            activeFilterSet = '';

        filterSets.filterSets.forEach(function(filterSet){
            if (brandId) {
                brandIndex = filterSet.brandIds.map(function(id){
                    return id;
                }).indexOf(Number(brandId));

                if (brandIndex != -1) {
                    activeFilterSet = filterSet;
                };
            };
        });

        if (activeFilterSet === '') {
            activeFilterSet = filterSets.filterSets[0];
        };

        var filters = activeFilterSet.filters,
            filtersValues = [];

        if (vtxctx.categoryId === "147" || vtxctx.categoryId === "177") {
            filters.unshift({
                id: 39,
                name: "Formato da Toalha",
                loadOpen: true
            });
        };

        filters.forEach(function(filter){
            if (filter.id != 'Price') {
                var filterValues = $.ajax({
                    crossDomain: true,
                    contentType: 'application/json',
                    url: "/api/catalog_system/pub/specification/fieldvalue/" + filter.id,
                    method: "GET"
                }).then(function(results){
                    filter.values = [];

                    results.forEach(function(result){
                        var value = {
                            filterId: filter.id,
                            id: result.FieldValueId,
                            value: result.Value
                        };

                        filter.values.push(value);
                    });
                });

                filtersValues.push(filterValues);
            };
        });

        var priceFilter = {
            id: 'Price',
            name: 'Preço',
            priceFilter: true,
            values: [
                {
                    filterId: 'Price',
                    text: 'R$1 - R$15',
                    value: '1-15'
                },
                {
                    filterId: 'Price',
                    text: 'R$15 - R$30',
                    value: '15-30'
                },
                {
                    filterId: 'Price',
                    text: 'R$30 - R$45',
                    value: '30-45'
                },
                {
                    filterId: 'Price',
                    text: 'R$45 - R$60',
                    value: '45-60'
                },
                {
                    filterId: 'Price',
                    text: 'R$60 - R$75',
                    value: '60-75'
                },
                {
                    filterId: 'Price',
                    text: 'R$75 - R$90',
                    value: '75-90'
                },
                {
                    filterId: 'Price',
                    text: 'R$90 - R$100',
                    value: '90-100'
                }
            ]
        };

        var priceFilterIndex = filters.map(function(filter){
            return filter.id;
        }).indexOf('Price');

        if (priceFilterIndex == -1) {
            filters.push(priceFilter);
        };

        $.when.apply($, filtersValues).done(function(){
            var data = {
                filters: filters,
                activeFilters: activeFilters
            };

            dust.render('catalog/filters', data, function(err, out){
                $('.filters__filters').html(out);

                $('[name="Branco"]').parent().css('background', '#fff');
                $('[name="Azul"]').parent().css('background', '#0184D0');
                $('[name="Amarelo"]').parent().css('background', '#FFE916');
                $('[name="Preto"]').parent().css('background', '#222');
                $('[name="Verde"]').parent().css('background', '#26D001');
                $('[name="Vermelho"]').parent().css('background', '#FF0322');
                $('[name="Laranja"]').parent().css('background', '#FF7F03');
                $('[name="Roxo"]').parent().css('background', '#A203FF');
                $('[name="Rosa"]').parent().css('background', '#FF58EC');
                $('[name="Dourado"]').parent().css('background', '#D5C90A');
                $('[name="Marrom"]').parent().css('background', '#964D06');
                $('[name="Bege"]').parent().css('background', '#DCB483');
                $('[name="Cinza"]').parent().css('background', '#616161');
                $('[name="Vinho"]').parent().css('background', '#D0011B');
                $('[name="Lilás"]').parent().css('background', '#C6A2FF');

                $('.form-check-input[data-filter-id="19"]').parent().tooltip();

                // $('.form-check-input[data-filter-id="19"]').change(function () {
                //     var $input = $(this);
                //     if ($(this).prop('checked', true)) {
                        
                //     } else
                // })

                // $('.form-check-input[data-filter-id="19"]').change(function () {
                //     if ($(this).prop("checked")) {
                //         $(this).css('border', '2px solid #95499D')
                //     } else {
                //         $(this).css('border', 'none')
                //     }
                // });
                
                if (queryParameters.filters.url){
                    var filtersParams = queryParameters.filters.url.split(';');

                    filtersParams.forEach(function(filterParam, index){
                        $('.filter__checkbox[name="' + filterParam.split('~')[1] + '"]').prop('checked', true);
                    });
                };

                $('.filter__checkbox').change(function(){
                    var filterId = $(this).data('filter-id'),
                        filterValue = $(this).data('filter-value');

                    if ($(this).prop('checked')) {
                        if (filterId === 'Price') {
                            $('.filter--' + filterId + ' input').prop('checked', false);
                            $(this).prop('checked', true);
                        };

                        if (helpers.getUrlParameter('F')) {
                            history.pushState(null, '', parameterizedUrl.replace(helpers.getUrlParameter('F'), helpers.getUrlParameter('F') + filterId + '~' + filterValue + ';'));
                        } else {
                            history.pushState(null, '', parameterizedUrl + '&F=' + filterId + '~' + filterValue + ';');
                        };
                    } else {
                        if ((parameterizedUrl.match(/;/g) || []).length > 1) {
                            history.pushState(null, '', parameterizedUrl.replace(filterId + '~' + filterValue + ';', ''));
                        } else {
                            history.pushState(null, '', parameterizedUrl.replace('&F=' + filterId + '~' + filterValue + ';', ''));
                        };
                    };

                    checkFilterParameters(helpers.getUrlParameter('F'));
                    checkPageParamenter(1);
                    updateQuery();
                });
            }); 
        });
    };
    

    var init = function() {
        // check current category

        if (vtxctx.categoryId){
            var getCurrentCategory = helpers.searchCategory(vtxctx.categoryId)
            .then(function(result) {
                var category = {
                    name: result.name,
                    ids: ''
                };

                if (result.parentId) {
                    if (result.parentId != vtxctx.departmentyId) {
                        category.ids = vtxctx.departmentyId + '/' + result.parentId + '/' + vtxctx.categoryId;
                    } else {
                        category.ids = vtxctx.departmentyId + '/' + vtxctx.categoryId;
                    };
                } else {
                    category.ids = vtxctx.departmentyId;
                };

                return category;
            }).fail(function(){
                console.log('falha ao buscar árvore de categorias');
            });
        } else {
            var getCurrentCategory = false;
        };


        // check current brand 

        var getCurrentBrand = helpers.searchBrands()
        .then(function(brands) {
            var categories = $('link[rel="canonical"]').attr('href').split('/').splice(3, Number.MAX_VALUE);

            var currentBrandIndex = brands.map(function(brand) {
                return helpers.convertToSlug(brand.name);
            }).indexOf(String(categories[categories.length -1]));

            if (currentBrandIndex != -1) {
                brands[currentBrandIndex].slug = helpers.convertToSlug(brands[currentBrandIndex].name);
                return brands[currentBrandIndex];
            } else {
                return false;
            };
        }).fail(function(){
            console.log('falha ao buscar listagem de marcas');
        });


        // check cluster parameters

        if (helpers.getUrlParameter('H')) {
            checkClusterParameters(helpers.getUrlParameter('H'));
        };


        // check ordering parameters

        if (helpers.getUrlParameter('O')) {
            checkOrderingParameters(helpers.getUrlParameter('O'));
        };


        // check page parameter

        if (helpers.getUrlParameter('P')) {
            checkPageParamenter(Number(helpers.getUrlParameter('P')));
        };


        // check filter parameters

        if (helpers.getUrlParameter('F')) {
            checkFilterParameters(helpers.getUrlParameter('F'));
        };

        $.when(getCurrentCategory, getCurrentBrand)
        .then(function(category, brand){
            if (brand) {
                filters(brand.id);
                $('.catalog-banner__heading').html('<h2 class="titulo-sessao">' + brand.name + '</h2');
                $('.catalog-banner__image').html('<img src="/arquivos/' + brand.slug.replace("-", "_") + '_banner.jpg">');
                $('.bread-crumb .last').removeClass('last').parent().append('<li class="last"><a href="' + canonicalUrl + '">' + brand.name + '</a></li>');
            } else {
                filters(false);
            };

            if (category) {
                if (category.ids.indexOf('/') != -1) {
                    var categoryId = category.ids.split('/'),
                        categoryId = categoryId[categoryId.length - 1];
                } else {
                    var categoryId = category.ids;
                };
            };

            if (vtxctx.searchTerm) {
                $('.catalog-banner__heading').html('<h2 class="titulo-sessao">' + vtxctx.searchTerm + '</h2');
            }

            if (category && brand) {
                queryParameters.category = category;
                queryParameters.brand = brand;
            } else if (category && !brand) {
                queryParameters.category = category;
            } else if (!category && brand) {
                queryParameters.brand = brand;
            };

            updateQuery();
        });
    };

    if ($('body').hasClass('catalog')) {       
        init();
    };
});