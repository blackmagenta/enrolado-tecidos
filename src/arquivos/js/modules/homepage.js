define(['helpers', 'flexslider'], function(helpers){
    $('.homepage__banner .box-banner').each(function () {
        $(this).wrap('<li></li>');
    });

    $('.homepage__banner').flexslider({
        animation: "slide",
        slideshow: true,
        slideshowSpeed: 5000
    });

    $('.flex-catalog').each(function(){
        var slider = $(this),
            items = slider.find('ul li:not(".helperComplement")');
        
        slider.append('<ul class="slides"></ul>');
        slider.find('.slides').append(items);
        slider.find('ul:not(".slides")').remove();
        slider.find('h2').addClass('homepage__heading');
    });

    var featuredTwoHeading = $('.homepage__featured--2 .homepage__heading');
    $('.homepage__featured--2').prepend(featuredTwoHeading);

    var featuredThreeHeading = $('.homepage__featured--3 .homepage__heading');
    $('.homepage__featured--3 > .row > .col-lg-6:first-child').prepend(featuredThreeHeading);

    $('.homepage__featured--1 .flex-catalog, .homepage__featured--4 .flex-catalog').flexslider({
        animation: "slide",
        slideshow: false,
        itemWidth: 263,
        itemMargin: 15,
        minItems: 2,
        maxItems: 5
    });

    $('.homepage__featured--2 .flex-catalog').flexslider({
        animation: "slide",
        slideshow: false,
        itemWidth: 263,
        itemMargin: 15,
        minItems: 2,
        maxItems: 4
    });

    $('.homepage__featured--3 .flex-catalog').flexslider({
        animation: "slide",
        slideshow: false,
        itemWidth: 263,
        itemMargin: 15,
        minItems: 2,
        maxItems: 3
    });

    $('.product-thumbnail').each(function(){
        var thumbnail = $(this),
            thumbnailSku = thumbnail.data('product-sku'),
            thumbnailImg = thumbnail.find('.product-thumbnail__image'),
            wholesalePriceContainer = thumbnail.find('.wholesale-price__price');

        helpers.searchProduct('?fq=skuId:' + thumbnailSku).then(function(results){
            var productItem = results[0].items[0];
            
            if (productItem.images[1]) {
                var secondImageId = productItem.images[1].imageId,
                    secondImageUrl = productItem.images[1].imageUrl.replace(secondImageId, secondImageId + '-300-300');

                thumbnailImg
                    .addClass('product-thumbnail__image--multiple')
                    .append('<img src="' + secondImageUrl + '" alt="" />');
            };
        });

        helpers.simulateProductPrice(thumbnailSku, 50, 1).then(function(product){
            var wholesalePrice;

            if (product.items[0].sellingPrice < product.items[0].price) {
                wholesalePrice = helpers.formatPrice(product.items[0].sellingPrice);
                
                if (product.items[0].measurementUnit === 'm') {
                    wholesalePrice += '/m';
                };

                wholesalePriceContainer.html(wholesalePrice);
            } else {
                wholesalePriceContainer.parent().hide();
                thumbnail.find('.product-thumbnail__price').removeClass('product-thumbnail__price--wholesale');
            };
        });
    });

    $('.product-thumbnail__meter-selector').change(function(){
        $(this).next().data('quantity', $(this).val());
    });
});