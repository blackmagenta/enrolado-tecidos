define([
    'helpers',
    'selector',
    'price-table',
    'flexslider',
    '../views/product-page/info',
    '../views/product-page/images',
    '../views/product-page/colors',
    '../views/product-page/colors-mobile',
    '../views/product-page/designs',
    '../views/product-page/shipping',
    '../views/product-page/payment',
    '../views/product/product-slider', 
    '../views/product/product-thumbnail',
], function(helpers, selector, priceTable){
    var renderProduct = function(product){
        dust.render('product-page/info', product, function(err, out){
            $('.product-info__info').prepend(out);
            
            if (product.availableQuantity == 0) {
                $('.product__out-of-stock').removeClass('d-none');
            };
        });

        dust.render('product-page/shipping', product.shipping, function(err, out){
            $('.product__shipping-info').html(out);
        });

        dust.render('product-page/payment', product, function(err, out){
            $('.product__payment-info').html(out);
        });

        dust.render('product-page/images', product, function(err, out){
            $('.product__images .product__images').html(out);
        });

        $('.product-images__thumbnails').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 100,
            itemMargin: 15,
            asNavFor: '.product-images__slider'
        });
        
        $('.product-images__slider').flexslider({
            animation: "slide",
            controlNav: false,
            directionNav: false,
            animationLoop: false,
            slideshow: false,
            sync: ".product-images__thumbnails"
        });
    };


    var renderProductOtherColors = function(results){
        if (results.length) {
            var productOtherColors = [];

            results.forEach(function(color){
                var imageUrl = color.items[0].images[0].imageUrl,
                    imageId = color.items[0].images[0].imageId,
                    imageUrlFormatted = imageUrl.replace(imageId, imageId+'-400-400.jpg'),
                    color = {
                        url: color.link,
                        name: color['Cor Predominante'],
                        imageUrl: imageUrlFormatted
                    };

                productOtherColors.push(color);
            });

            productOtherColors.sort(function(a, b){
                if (a.name == b.name) {
                    return 1;
                } else {
                    return -1;
                };
            });

            dust.render('product-page/colors', { productOtherColors: productOtherColors }, function(err, out){
                $('.product__other-colors').html(out);
            });

            dust.render('product-page/colors-mobile', { productOtherColors: productOtherColors }, function(err, out){
                $('.product__other-colors--mobile').html(out);
            });
        };
    };


    var relatedProducts = function(results, container){
        if (results.length) {
            var wholesalePrices = [];

            results.forEach(function(result){
                var item = result.items[0],
                    commertialOffer = item.sellers[0].commertialOffer;
                
                result.sku = item.itemId;
                result.imageUrl = item.images[0].imageUrl.replace(item.images[0].imageId, item.images[0].imageId + '-350-350');
                result.seller = item.sellers[0].sellerId;
                result.availableQuantity = commertialOffer.AvailableQuantity;
                result.isInStock = (result.availableQuantity > 0);
                result.listPrice = commertialOffer.ListPrice;
                result.listPriceFormatted = defaultStoreCurrency + (result.listPrice.toFixed(2).replace('.', ','));
                result.price = commertialOffer.Price;
                result.priceFormatted = defaultStoreCurrency + (result.price.toFixed(2).replace('.', ','));

                if (item.images[1]) {
                    result.secondImageUrl = item.images[1].imageUrl.replace(item.images[1].imageId, item.images[1].imageId + '-500-500');
                };

                if (result.listPrice > result.price) {
                    result.hasBestPrice = true;
                }

                if (result['Tipo de Seletor']) {
                    result.selector = result['Tipo de Seletor'][0];
                };

                if (result.selector === 'Seletor de Metro') {
                    result.meterSelector = true;
                    result.priceFormatted += '/m';
                } else if (result.selector === 'SKU (exemplo tamanho e cor)') {
                    result.skuSelector = true;
                } else {
                    result.quantitySelector = true;
                };

                var wholesalePrice = helpers.simulateProductPrice(result.sku, 50, result.seller).then(function(product){
                    if (item.listPrice < item.price) {
                        result.wholeSale = true;
                        result.wholesalePrice = helpers.formatPrice(item.listPrice) + '/m';
                    } else {
                        result.wholeSale = false;
                    };
                });

                wholesalePrices.push(wholesalePrice);
            });

            $.when.apply($, wholesalePrices).then(function(){
                dust.render('product/product-slider', { products: results }, function(err, out){
                    container.html(out);

                    $(container).find('.product-thumbnail').each(function(index){
                        var thumbnail = $(this),
                            thumbnailSku = thumbnail.data('product-sku'),
                            thumbnailImg = thumbnail.find('.product-thumbnail__image'),
                            wholesalePriceContainer = thumbnail.find('.wholesale-price__price');
    
                        if (results[index].secondImageUrl) {
                            thumbnailImg
                                .addClass('product-thumbnail__image--multiple')
                                .append('<img src="' + results[index].secondImageUrl + '" alt="" />');
                        };
    
                        helpers.simulateProductPrice(thumbnailSku, 50, 1).then(function(product){
                            var wholesalePrice,
                                sellingPrice = product.items[0].sellingPrice,
                                price = product.items[0].price,
                                measurementUnit = product.items[0].measurementUnit;
                
                            if (sellingPrice < price) {
                                wholesalePriceContainer.html(helpers.formatPrice(sellingPrice) + '/' + measurementUnit);
                            } else {
                                wholesalePriceContainer.parent().hide();
                                thumbnail.find('.product-thumbnail__price').removeClass('product-thumbnail__price--wholesale');
                            };
                        });
                    });

                    $(container).find('.flex-catalog').flexslider({
                        animation: "slide",
                        slideshow: false,
                        itemWidth: 300,
                        itemMargin: 15,
                        minItems: 2,
                        maxItems: 5
                    });
                });
            });
        } else {
            container.parent().parent().hide();
        };
    };


    if ($('body').hasClass('product')) {
        helpers.loading(true);

        var productUrl = window.location.href.split(window.location.hostname)[1].toLowerCase(),
            product = {};
            
        helpers.searchProduct(productUrl).done(function(results){
            var product = results[0];
            product.item = product.items[0];

            product.id = product.productId;
            product.items = product.items;
            product.url = product.link;
            product.name = product.productName;
            product.brand = product.brand;
            product.description = $.parseHTML(String(product.description));
            product.sku = product.item.itemId;
            product.isKit = product.item.isKit;
            product.availableQuantity = product.item.sellers[0].commertialOffer.AvailableQuantity;
            product.listPrice = Number(product.item.sellers[0].commertialOffer.ListPrice.toFixed(2).replace('.', ''));
            product.listPriceFormatted = defaultStoreCurrency + (product.item.sellers[0].commertialOffer.ListPrice.toFixed(2).replace('.', ','));
            product.price = Number(product.item.sellers[0].commertialOffer.Price.toFixed(2).replace('.', ''));
            product.priceFormatted = defaultStoreCurrency + (product.item.sellers[0].commertialOffer.Price.toFixed(2).replace('.', ','));
            product.seller = product.item.sellers[0].sellerId;
            product.wholeSale = false;
            product.quantitySelector = true;
            product.color = product['Cor Predominante'];
            product.otherColors = product['Outras cores'];
            product.tone = product['Tom Predominante'];
            product.design = product['Desenho'];
            product.shipping = {
                price: '-',
                deadline: '-'
            };

            if (!product.availableQuantity) {
                product.availableQuantity = false;
            };

            if (product.listPrice > product.price) {
                product.hasBestPrice = true;
            };
        
            if (product.item.images.length) {
                product.images = [];
        
                product.item.images.forEach(function(image) {
                    var image = {
                        url: image.imageUrl.replace(image.imageId, image.imageId+'-600-600.jpg'),
                        name: image.imageLabel
                    };
        
                    product.images.push(image);
                });
            };


            // check if is kit

            if (product.isKit) {
                var kitItemsParameters = [];
                
                product.kitItems = [];
                product.componentsTotal = 0;
                product.quantitySelector = false;

                product.item.kitItems.forEach(function(kitItem, index){
                    var prefix = (index != 0) ? '&fq=skuId:' : '?fq=skuId:';

                    kitItemsParameters.push(prefix + kitItem.itemId);
                });

                helpers.searchProduct(kitItemsParameters).then(function(results){
                    results.forEach(function(result){
                        result.id = result.productId;
                        result.name = result.productName;
                        result.sku = result.items[0].itemId;
                        result.availableQuantity = result.items[0].sellers[0].commertialOffer.AvailableQuantity;
                        result.listPrice = Number(result.items[0].sellers[0].commertialOffer.ListPrice.toFixed(2).replace('.', ''));
                        result.listPriceFormatted = defaultStoreCurrency + (result.items[0].sellers[0].commertialOffer.ListPrice.toFixed(2).replace('.', ','));
                        result.price = Number(result.items[0].sellers[0].commertialOffer.Price.toFixed(2).replace('.', ''));
                        result.priceFormatted = defaultStoreCurrency + (result.items[0].sellers[0].commertialOffer.Price.toFixed(2).replace('.', ','));
                        result.seller = result.items[0].sellers[0].sellerId;

                        if (result.listPrice > result.price) {
                            result.hasBestPrice = true;
                        };

                        if (result.items[0].images.length) {
                            result.image = {
                                url: result.items[0].images[0].imageUrl.replace(result.items[0].images[0].imageId, result.items[0].images[0].imageId+'-200-200.jpg'),
                                name: result.items[0].images[0].imageLabel
                            };
                        };

                        product.componentsTotal += result.items[0].sellers[0].commertialOffer.Price;
                        product.kitItems.push(result);
                    });

                    product.componentsTotal = helpers.formatPrice(product.componentsTotal.toFixed(2).replace('.', ''));

                    renderProduct(product);
                    helpers.loading(false);
                }).fail(function(){
                    alert('Falha ao carregar cores relacionadas, por favor, recarregue a página.');
                });
            } else {
                if (product.items.length > 1) {
                    product.name += ' ' + product.items[0].name;
                    product.skus = [];
    
                    product.items.forEach(function(item, index){
                        var sku = {
                            id: item.itemId,
                            name: item.name,
                            isSelected: (index === 0) ? true : false
                        };
    
                        product.skus.push(sku);
                    });
                };

                if (product['Tipo de Seletor'] && product.availableQuantity) {
                    product.selector = product['Tipo de Seletor'][0];
                };
    
                if (product.selector === 'Seletor de Metro' && product.availableQuantity) {
                    product.meterSelector = true;
                    product.priceFormatted += '/m';
                    product.quantitySelector = false;
                } else if (product.selector === 'SKU (exemplo tamanho e cor)' && product.availableQuantity) {
                    product.skuSelector = true;
                };

                helpers.simulateProductPrice(product.sku, 50, product.seller).done(function(result){
                    var resultItem = result.items[0];
    
                    if (product.availableQuantity) {
                        if (resultItem.sellingPrice < resultItem.price) {
                            product.wholeSale = true;
                            product.wholesalePrice = helpers.formatPrice(resultItem.sellingPrice);
                        };
                    };
    
                    renderProduct(product);
    
                    $.ajax({
                        crossDomain: true,
                        url: "/api/dataentities/SA/search",
                        method: "GET",
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/vnd.vtex.ds.v10+json",
                            "REST-Range": "resources=0-49",
                            "X-VTEX-API-AppKey": "vtexappkey-enroladotecidos-KVZFFN",
                            "X-VTEX-API-AppToken": "PJYUHXZOWTMZGTPAHTZOURAOONKQSUZGMNKDFPSYVFQSDODPVHQRKGKYIKFTMIWCTFMSIMVSBASOBIKAJVVKDIWFCMVVKZLMJNZYCLHJMYQJOVWZZFJRNMZKJGPVQAIB"
                        }
                    }).then(function(response) {
                        var productIndex = response.map(function(simulation){
                            return simulation.link;
                        }).indexOf(productUrl);
            
                        if (productIndex != -1) {
                            $('.product__simulator-banner .btn--ambient-simulator').attr('href', '/guia-de-compras/simulador-de-ambientes?id=' + response[productIndex].id);
                            $('.product__simulator-banner').fadeIn();
                        };
                    });
                    
                    helpers.loading(false);
                    
                    selector(product);
    
                    if (product.wholeSale) {
                        priceTable(product);
                    };
                });
            };

            if (vtxctx.postalCode && product.availableQuantity) {
                var items = [{
                    id: $('.btn--buy').data('sku'),
                    quantity: $('.btn--buy').data('quantity'),
                    seller: $('.btn--buy').data('seller')
                }];

                helpers.simulateProductShipping(items, vtxctx.postalCode, localeInfo.CountryCode)
                .done(function(orderForm){
                    var slas = orderForm.logisticsInfo[0].slas,
                        deadlines = [];

                    slas.forEach(function(sla){
                        deadlines.push(Number(sla.shippingEstimate.replace('bd', '')));
                    });

                    var fastestDeadline = Math.min.apply(null, deadlines);

                    var fastestSlaIndex = slas.map(function(sla){
                        return Number(sla.shippingEstimate.replace('bd', ''));
                    }).indexOf(fastestDeadline);

                    var fastestSla = orderForm.logisticsInfo[0].slas[fastestSlaIndex];

                    var data = {
                        price: helpers.formatPrice(fastestSla.price),
                        deadline: 'Até ' + fastestSla.shippingEstimate.replace('bd', '') + ' Dias',
                        info: true
                    };
            
                    dust.render('product-page/shipping', data, function(err, out){
                        $('.product-info__shipping').html(out);
                    });
                });
            };


            // other designs and similar products

            product.otherDesigns = [];

            $.ajax({
                crossDomain: true,
                url: "/api/catalog_system/pub/products/crossselling/similars/" + product.id,
                method: "GET",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                }
            }).then(function(results){
                if (results.length) {
                    results.forEach(function(similarProduct){
                        var firstSku = similarProduct.items[0],
                            imageUrl = firstSku.images[0].imageUrl,
                            imageId = firstSku.images[0].imageId,
                            imageUrlFormatted = imageUrl.replace(imageId, imageId+'-400-400.jpg'),
                            design = {
                                url: similarProduct.link,
                                name: similarProduct['Desenho'],
                                imageUrl: imageUrlFormatted
                            };

                        product.otherDesigns.push(design);
                    });

                    dust.render('product-page/designs', { otherDesigns: product.otherDesigns }, function(err, out){
                        $('.product__other-designs').html(out);
                    });
                };

                relatedProducts(results, $('.similar-products__shelf'));
            });


            // other colors

            if (product.otherColors) {
                product.otherColors = product.otherColors[0].split(',');

                var otherColorsIds = [];

                product.otherColors.forEach(function(otherColorId, index){
                    var prefix = (index != 0) ? '&fq=productId:' : '?fq=productId:';

                    otherColorsIds.push(prefix + otherColorId);
                });

                helpers.searchProduct(otherColorsIds).then(function(results){
                    renderProductOtherColors(results);
                }).fail(function(){
                    alert('Falha ao carregar cores relacionadas, por favor, recarregue a página.');
                });
            } else {
                var designFilterId = '&fq=specificationFilter_21:';

                $.ajax({
                    crossDomain: true,
                    contentType: "application/json",
                    url: '/api/catalog_system/pvt/brand/list',
                    method: "GET",
                    headers: {
                        "X-VTEX-API-AppKey": "vtexappkey-enroladotecidos-KVZFFN",
                        "X-VTEX-API-AppToken": "PJYUHXZOWTMZGTPAHTZOURAOONKQSUZGMNKDFPSYVFQSDODPVHQRKGKYIKFTMIWCTFMSIMVSBASOBIKAJVVKDIWFCMVVKZLMJNZYCLHJMYQJOVWZZFJRNMZKJGPVQAIB"
                    }
                }).then(function(results) {
                    var currentBrandIndex = results.map(function(brand) {
                        return brand.name;
                    }).indexOf(String(product.brand));

                    product.brandId = results[currentBrandIndex].id;

                    helpers.searchProduct('?fq=C:' + vtxctx.departmentyId + '/' + vtxctx.categoryId + '&fq=B:' + product.brandId + designFilterId + product.design + '&_from=0&_to=19')
                    .done(function(results){
                        renderProductOtherColors(results);
                    }).fail(function(){
                        alert('Falha ao carregar cores relacionadas, por favor, recarregue a página.');
                    });
                }).fail(function(){
                    alert('Falha ao carregar cores relacionadas, por favor, recarregue a página.');
                });
            };


            // product accessories

            $.ajax({
                crossDomain: true,
                url: "/api/catalog_system/pub/products/crossselling/accessories/" + product.id,
                method: "GET",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                }
            }).done(function(results) {
                relatedProducts(results, $('.accessories__shelf'));
            });
        }).fail(function(){
            alert('Falha ao carregar produto, por favor, recarregue a página.');
        });
    };
});