define(['helpers', '../views/product-page/shipping'], function(helpers){
    var shippingForm = $('.shipping__form'),
        shippingInput = $('.shipping__input'),
        shippingButton = $('.btn--shipping');

    if ($('body').hasClass('product')) {
        var isProductPage = true;
    };

    vtexjs.checkout.getOrderForm()
    .done(function(orderForm){
        if (orderForm.shippingData) {
            if (orderForm.shippingData.address) {
                if (orderForm.shippingData.address.postalCode) {
                    vtxctx.postalCode = orderForm.shippingData.address.postalCode;
        
                    shippingInput.val(vtxctx.postalCode);
                    shippingButton.html('Frete: ' + vtxctx.postalCode);

                    if (isProductPage) {
                        var items = [{
                            id: $('.btn--buy').data('sku'),
                            quantity: $('.btn--buy').data('quantity'),
                            seller: $('.btn--buy').data('seller')
                        }];
                    
                        helpers.simulateProductShipping(items, vtxctx.postalCode, localeInfo.CountryCode)
                        .done(function(orderForm){
                            var slas = orderForm.logisticsInfo[0].slas,
                                deadlines = [];

                            slas.forEach(function(sla){
                                deadlines.push(Number(sla.shippingEstimate.replace('bd', '')));
                            });

                            var fastestDeadline = Math.min.apply(null, deadlines);

                            var fastestSlaIndex = slas.map(function(sla){
                                return Number(sla.shippingEstimate.replace('bd', ''));
                            }).indexOf(fastestDeadline);

                            var fastestSla = orderForm.logisticsInfo[0].slas[fastestSlaIndex];

                            var data = {
                                price: helpers.formatPrice(fastestSla.price),
                                deadline: 'Até ' + fastestSla.shippingEstimate.replace('bd', '') + ' Dias',
                                info: true
                            };
                    
                            dust.render('product-page/shipping', data, function(err, out){
                                $('.product__shipping-info').html(out);
                            });
                        });
                    };
                };
            };
        };
    });

    shippingForm.submit(function(e){
        e.preventDefault();

        vtxctx.postalCode = shippingInput.val();

        if (vtxctx.postalCode !== '00000-000' && vtxctx.postalCode.length === 9) {
            vtexjs.checkout.getOrderForm()
            .then(function(orderForm){
                var address = {
                    postalCode: vtxctx.postalCode,
                    country: localeInfo.CountryCode,
                };

                return vtexjs.checkout.calculateShipping(address);
            }).done(function(orderForm){
                shippingInput.val(vtxctx.postalCode);
                shippingButton.html('Frete: ' + vtxctx.postalCode);
                helpers.alert(shippingForm, 'success', 'Pronto. A partir de agora todos os produtos acessados exibirão o frete e o prazo de entrega. No carrinho de compras você poderá escolher entre outras opções de entrega.');

                $('.shipping__ok').prop('disabled', false);

                if (isProductPage) {
                    var items = [{
                        id: $('.btn--buy').data('sku'),
                        quantity: $('.btn--buy').data('quantity'),
                        seller: $('.btn--buy').data('seller')
                    }];

                    helpers.simulateProductShipping(items, vtxctx.postalCode, localeInfo.CountryCode)
                    .done(function(orderForm){
                        var slas = orderForm.logisticsInfo[0].slas,
                            deadlines = [];

                        slas.forEach(function(sla){
                            deadlines.push(Number(sla.shippingEstimate.replace('bd', '')));
                        });

                        var fastestDeadline = Math.min.apply(null, deadlines);

                        var fastestSlaIndex = slas.map(function(sla){
                            return Number(sla.shippingEstimate.replace('bd', ''));
                        }).indexOf(fastestDeadline);

                        var fastestSla = orderForm.logisticsInfo[0].slas[fastestSlaIndex];

                        var data = {
                            price: helpers.formatPrice(fastestSla.price),
                            deadline: 'Até ' + fastestSla.shippingEstimate.replace('bd', '') + ' Dias',
                            info: true
                        };
                
                        dust.render('product-page/shipping', data, function(err, out){
                            $('.product__shipping-info').html(out);
                        });
                    });
                };
            }).fail(function(orderForm){
                helpers.alert(shippingForm, 'danger', 'Falha ao salvar CEP, verifique o CEP Informado e tente novamente.');
            });
        } else {
            helpers.alert(shippingForm, 'danger', 'CEP Inválido');
        };
    });
});