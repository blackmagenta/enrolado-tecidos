define(['helpers', 'price-table', 'jqueryui', 'jqueryuitouch', '../views/product-page/shipping'], function(helpers, priceTable, slide){
    return function(product){
        var buyButton = $('.btn--buy'),
            prodcutPriceContainer = $('.product-info__meter-price span'),
            progressiveDiscountClass = 'product-info__meter-price--discount';

        var updateProductPrice = function(item, oldPrice){
            return helpers.simulateProductPrice(item.sku, item.quantity, item.seller).done(function(result){
                var newProductPrice = result.items[0].sellingPrice,
                    newProductPriceFormatted = helpers.formatPrice(result.items[0].sellingPrice);

                if (buyButton.data('cloth')) {
                    newProductPriceFormatted += '/m';
                };

                if (newProductPrice < oldPrice) {
                    prodcutPriceContainer.parent().addClass(progressiveDiscountClass);
                } else {
                    prodcutPriceContainer.parent().removeClass(progressiveDiscountClass);
                };

                prodcutPriceContainer
                    .animate({opacity: 0}, 350)
                    .html(newProductPriceFormatted)
                    .animate({opacity: 1}, 350);
            }).fail(function(result){
                console.log('Falha na chamada Ajax à API da VTEX');
            });
        };

        var updateShippingPrice = function(){
            if (vtxctx.postalCode) {
                var items = [{
                    id: $('.btn--buy').data('sku'),
                    quantity: $('.btn--buy').data('quantity'),
                    seller: product.seller
                }];

                helpers.simulateProductShipping(items, vtxctx.postalCode, localeInfo.CountryCode)
                .done(function(orderForm){
                    var slas = orderForm.logisticsInfo[0].slas,
                            deadlines = [];

                    slas.forEach(function(sla){
                        deadlines.push(Number(sla.shippingEstimate.replace('bd', '')));
                    });

                    var fastestDeadline = Math.min.apply(null, deadlines);

                    var fastestSlaIndex = slas.map(function(sla){
                        return Number(sla.shippingEstimate.replace('bd', ''));
                    }).indexOf(fastestDeadline);

                    var fastestSla = orderForm.logisticsInfo[0].slas[fastestSlaIndex];

                    var data = {
                        price: helpers.formatPrice(fastestSla.price),
                        deadline: 'Até ' + fastestSla.shippingEstimate.replace('bd', '') + ' Dias',
                        info: true
                    };
            
                    dust.render('product-page/shipping', data, function(err, out){
                        $('.product__shipping-info').html(out);
                    });
                });
            };
        };

        if (product.meterSelector === true){
            var sliderElement = $('#slider'),
                handler = $('.meter-selector__handler'),
                input = $('.meter-selector__input'),
                availableQuantity = Number(product.availableQuantity),
                quantity = 1;

            if (availableQuantity < 50) {
                var maxRange = availableQuantity;
            } else {
                var maxRange = 50;
            };

            sliderElement.slider({
                range: "max",
                min: 1,
                max: maxRange,
                value: 1,
                slide: function(event, ui) {
                    handler.html(ui.value);
                    input.val(ui.value).attr('value', ui.value);
                    buyButton.data('quantity', ui.value);
                },
                change: function(event, ui){
                    updateMeterPrice(product, ui.value);
                }
            });
            
            sliderElement.attr('data-content', maxRange+'m');
            input.attr('max', availableQuantity);

            input.change(function(){
                quantity = Number($(this).val());

                if (quantity < 1) {
                    sliderElement.slider('value', 1);
                    $(this).val(1).attr('value', 1);
                    buyButton.data('quantity', 1);

                    updateMeterPrice(product, 1);
                } else if (quantity > availableQuantity) {
                    sliderElement.slider("value", availableQuantity);
                    $(this).val(availableQuantity).attr('value', availableQuantity);
                    buyButton.data('quantity', availableQuantity);

                    updateMeterPrice(product, availableQuantity);
                } else {
                    sliderElement.slider("value", quantity);
                    $(this).val(quantity).attr('value', quantity);
                    buyButton.data('quantity', quantity);
                    
                    updateMeterPrice(product, quantity);
                };

                if (quantity <= 50 ){
                    handler.html(quantity);
                } else {
                    handler.html('50+');
                }
            });

            var updateMeterPrice = function(product, quantity){
                var item = {
                    sku: product.sku,
                    quantity: quantity,
                    seller: product.seller
                };

                updateProductPrice(item, product.price);
                updateShippingPrice([item]);
            };
        } else {
            var quantityInput = $('.product__quantity-selector .quantity__value'),
                quantityAdd = $('.product__quantity-selector .quantity__add'),
                quantitySubtract = $('.product__quantity-selector .quantity__remove'),
                quantityMin = Number(quantityInput.attr('min')),
                quantityMax = Number(quantityInput.attr('max')),
                quantityVal = Number(quantityInput.val());
            
            var updateBuyButtonLink = function(quantity){
                buyButton.data('quantity', quantity);

                var item = {
                    sku: $('.btn--buy').data('sku'),
                    quantity: quantity,
                    seller: product.seller
                };

                updateProductPrice(item, product.price);
                updateShippingPrice([item]);
            };

            var add = function(){
                quantityInput.val(function(i, oldval){
                    if (oldval < quantityMax) {
                        return ++oldval;
                    } else {
                        return oldval;
                    };
                });
                
                updateBuyButtonLink(Number(quantityInput.val()));
            };

            var subtract = function(){
                quantityInput.val(function(i, oldval){
                    if (oldval > quantityMin) {
                        return --oldval;
                    } else {
                        return oldval;
                    }
                });

                updateBuyButtonLink(Number(quantityInput.val()));
            };

            var change = function(quantityVal){
                if (quantityVal !== 0 && quantityVal <= quantityMax) {
                    quantityInput.val(quantityVal);
                } else if (quantityVal === 0) {
                    quantityInput.val(1);
                } else {
                    quantityInput.val(quantityMax);
                };
                
                updateBuyButtonLink(Number(quantityInput.val()));
            };

            quantityAdd.click(function(){
                add();
            });

            quantitySubtract.click(function(){
                subtract();
            });

            quantityInput.change(function(){
                quantityVal = Number($(this).val());
                change(quantityVal);
            });

            if (product.skuSelector === true) {
                var skuItem = $('.sku-selector__item');
    
                skuItem.click(function(){
                    var sku = String($(this).data('id')),
                        skuPrice,
                        skuWholeSalePrice;
                    
                    skuItem.removeClass('sku-selector__item--selected');
                    $(this).addClass('sku-selector__item--selected');

                    var skuIndex = product.items.map(function(item) {
                        return item.itemId;
                    }).indexOf(sku);

                    $('.product-info__name').html(product.items[skuIndex].nameComplete);
                    $('.product-info__price--old span').html(helpers.formatPrice(Number(product.items[skuIndex].sellers[0].commertialOffer.ListPrice.toFixed(2).replace('.', ''))));
                    $('.product-info__meter-price span').html(helpers.formatPrice(Number(product.items[skuIndex].sellers[0].commertialOffer.Price.toFixed(2).replace('.', ''))));
                    $('.quantity-selector .quantity__value').attr('max', product.items[skuIndex].sellers[0].commertialOffer.AvailableQuantity);
                    buyButton.data('sku', sku);

                    if (product.wholeSale) {
                        product.sku = sku;
                        priceTable(product);
                    };

                    helpers.simulateProductPrice(sku, 1, product.seller)
                    .done(function(result){
                        skuPrice = result.items[0].sellingPrice;

                        helpers.simulateProductPrice(sku, 50, product.seller)
                        .done(function(result){
                            skuWholeSalePrice = result.items[0].sellingPrice;
                            $('.wholesale__price').html(helpers.formatPrice(skuWholeSalePrice));

                            updateProductPrice(item, skuPrice);

                            helpers.simulateProductPrice(sku, $('.btn--buy').data('quantity'), product.seller)
                            .done(function(result){
                                var newProductPrice = result.items[0].sellingPrice,
                                    newProductPriceFormatted = helpers.formatPrice(result.items[0].sellingPrice);

                                if (newProductPrice < skuPrice) {
                                    prodcutPriceContainer.parent().addClass(progressiveDiscountClass);
                                } else {
                                    prodcutPriceContainer.parent().removeClass(progressiveDiscountClass);
                                };

                                prodcutPriceContainer
                                    .animate({opacity: 0}, 350)
                                    .html(newProductPriceFormatted)
                                    .animate({opacity: 1}, 350);
                            }).fail(function(result){
                                console.log('Falha na chamada Ajax à API da VTEX');
                            });
                        }).fail(function(result){
                            console.log('Falha na chamada Ajax à API da VTEX');
                        });
                    }).fail(function(result){
                        console.log('Falha na chamada Ajax à API da VTEX');
                    });

                    var item = {
                        sku: sku,
                        quantity: $('.btn--buy').data('quantity'), 
                        seller: product.seller
                    };

                    updateShippingPrice([item]);
                });
            };
        };
    };
});