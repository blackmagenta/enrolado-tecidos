define(['helpers', '../views/product-page/price-table'], function(helpers){
    return function(product){
        var loopControl = 0,
            priceTableTrigger = $('.pricing-table__btn'),
            availableQuantity = product.availableQuantity,
            priceTable = {
                prices: []
            };
        
        if (availableQuantity < 50) {
            loopControl = availableQuantity;
        } else {
            loopControl = 50;
        };

        priceTableTrigger.click(function(){
            helpers.loading(true);

            $('.pricing-table__table').empty();
            priceTable.prices = [];

            var priceSimulations = [],
                unorderedPrices = [];

            for (let i = 0; i < loopControl; i++) {
                var priceSimulation = helpers.simulateProductPrice(product.sku, i + 1, product.seller).done(function(result){
                    var price = {
                        quantity: i + 1,
                        price: result.items[0].sellingPrice
                    };

                    unorderedPrices.push(price);
                });

                priceSimulations.push(priceSimulation);
            };

            $.when.apply($, priceSimulations).then(function(){
                var orderedPrices = [],
                    loopControl = unorderedPrices.length;
                
                for (let i = 0; i < loopControl; i++) {
                    var index = unorderedPrices.findIndex(function(index) { 
                        index.quantity === i + 1
                    });
                    orderedPrices.push(unorderedPrices[index]);
                };

                var sortedPrices = [],
                    loopControl = orderedPrices.length;

                orderedPrices.forEach(function(orderedPrice, i){
                    var sortedPrice = {
                        quantity: orderedPrice.quantity,
                        price: orderedPrice.price,
                        discount: true
                    };

                    if (i < loopControl - 1 && orderedPrice.price > orderedPrices[i+1].price || i === loopControl - 1) {
                        sortedPrices.push(sortedPrice);
                    };
                });

                sortedPrices[0].discount = false;

                sortedPrices.forEach(function(sortedPrice, i){
                    if (product.quantitySelector === true) {
                        var sufix = 'un';
                    } else {
                        var sufix = 'm';
                    };

                    var quantityMin = 'De 1' + sufix,
                        quantityMax = ' a ' + sortedPrice.quantity  + sufix,
                        price = helpers.formatPrice(sortedPrice.price) + '/' + sufix;
                    
                    if (i > 0 && i < sortedPrices.length - 1) {
                        quantityMin = 'De ' + (sortedPrices[i-1].quantity + 1) + sufix;
                    } else if (i === (sortedPrices.length - 1)) {
                        quantityMin = 'A partir de ' + sortedPrice.quantity + sufix;
                        quantityMax = '';
                    }
                    
                    var price = {
                        quantity: quantityMin + quantityMax,
                        price: price,
                        discount: sortedPrice.discount
                    };

                    priceTable.prices.push(price);
                });

                dust.render('product-page/price-table', priceTable, function(err, out){
                        $('#pricing-table .container-fluid').html(out);
                        helpers.loading(false);
                    });
            }).fail(function(){
                helpers.loading(false);
                window.alert('Falha ao obter dados, por favor tente novamente em instantes.');
            });
        });
    };
});