define(['helpers', '../mocks/navigation-tree', '../mocks/themes', '../views/header/mobile-menu', '../views/header/desktop-menu', 'flexslider'], function(helpers, navigationTree, themes){
    navigationTree.menu.forEach(function(menu){
        menu.slug = helpers.convertToSlug(menu.name);
        
        menu.children.forEach(function(children){
            children.slug = helpers.convertToSlug(children.name);
            children.menuBannerUrl = '/arquivos/' + children.slug + '_banner_menu.jpg';

            children.children.forEach(function(children){
                children.slug = helpers.convertToSlug(children.name);
            });
        });

        menu.menuBannerUrl = '/arquivos/' + menu.slug + '_banner_menu.jpg';
    });

    dust.render('header/mobile-menu', navigationTree, function(err, out){
        $('.mobile-menu').html(out);

        var menu = $('.mobile-menu-estatico'),
            menuLevel = $('.mobile-menu__layer'),
            menuToggler = $('.mobile-menu__button'),
            menuFirstLevel = $('.mobile-menu__first-level'),
            activeMenuLevelClass = 'mobile-menu__layer--active',
            activeMenuTogglerClass = 'mobile-menu__button--active',
            activeMenuClass = 'mobile-menu--active';

        var menuLink = $('.product-links__link[href^="#"]'),
            goBackLink = $('.mobile-menu__go-back');

        menuToggler.click(function(){
            toggle();
        });
        
        menuLink.click(function(){
            var submenu = $($(this).attr('href'));
            openSubmenu(submenu);
        });

        goBackLink.click(function(){
            var activeMenu = $(this).parent();
            goBack(activeMenu);
        });

        var toggle = function(){
            menuToggler.toggleClass(activeMenuTogglerClass);
            menu.toggleClass(activeMenuClass);

            if (menuLevel.hasClass(activeMenuLevelClass)) {
                menuLevel.removeClass(activeMenuLevelClass);
            } else {
                menuFirstLevel.toggleClass(activeMenuLevelClass);
                
            };
        };

        var openSubmenu = function(submenu){
            submenu.addClass(activeMenuLevelClass);
        };

        var goBack = function(activeMenu){
            activeMenu.removeClass(activeMenuLevelClass);
        };
    });

    dust.render('header/desktop-menu', navigationTree, function(err, out){
        $('.desktop-header .menu').html(out);

        $('.desktop-header .menu__link, .menu__dropdown').not('.menu__link--guia-de-compras').hover(function () {
            $('body').toggleClass('menu--active');
        });

        $('.desktop-header .menu__link[href^="#"], .menu-dropdown__list').hover(function(){
            $('.menu-image__container').css('background-image', 'url("/arquivos/' + $(this).data('category-slug') + '_banner_menu.jpg")');
        });

        if (vtxctx.departmentName) {
            var categorySlug = helpers.convertToSlug(vtxctx.departmentName);
            
            if (categorySlug != '') {
                $('.menu__link[data-category-slug="' + categorySlug + '"]').addClass('menu__link--active');
            };
        };

        if ($('body').hasClass('buying-guide')) {
            $('.menu__link--guia-de-compras').addClass('menu__link--active');
        };
    });

    
    $(window).scroll(function(){
        if ($(window).scrollTop() > 200) {
            $('.desktop-header').addClass('desktop-header--minimized');
        } else {
            $('.desktop-header').removeClass('desktop-header--minimized');
        };
    });


    // colors

    $.ajax({
        crossDomain: true,
        contentType: 'application/json',
        url: "/api/catalog_system/pub/specification/fieldvalue/19",
        method: "GET"
    }).done(function(results){
        $('.menu__dropdown--cores-e-temas .cores__list').empty();
        
        results.forEach(function(color){
            $('.menu__dropdown--cores-e-temas .cores__list').append('<li class="cores__item cores__item--' + color.FieldValueId + '" data-name="' + color.Value + '"><a href="/tecidos-para-decoracao/?O=OrderByReleaseDateDESC&F=19~' + color.Value + ';" class="cores__link" title="' + color.Value + '">' + color.Value + '</a></li>');
            $('.color-links').append('<a data-name="' + color.Value + '" href="/tecidos-para-decoracao/?O=OrderByReleaseDateDESC&F=19~' + color.Value + ';" class="cores__link color-links__link" title="' + color.Value + '">' + color.Value + '</a>');
        });

        $('.cores__link[data-name="Branco"], .cores__item[data-name="Branco"]').css('background', '#ffffff');
        $('.cores__link[data-name="Azul"], .cores__item[data-name="Azul"]').css('background', '#0184D0');
        $('.cores__link[data-name="Amarelo"], .cores__item[data-name="Amarelo"]').css('background', '#FFE916');
        $('.cores__link[data-name="Preto"], .cores__item[data-name="Preto"]').css('background', '#222222');
        $('.cores__link[data-name="Verde"], .cores__item[data-name="Verde"]').css('background', '#26D001');
        $('.cores__link[data-name="Vermelho"], .cores__item[data-name="Vermelho"]').css('background', '#FF0322');
        $('.cores__link[data-name="Laranja"], .cores__item[data-name="Laranja"]').css('background', '#FF7F03');
        $('.cores__link[data-name="Roxo"], .cores__item[data-name="Roxo"]').css('background', '#A203FF');
        $('.cores__link[data-name="Rosa"], .cores__item[data-name="Rosa"]').css('background', '#FF58EC');
        $('.cores__link[data-name="Dourado"], .cores__item[data-name="Dourado"]').css('background', '#D5C90A');
        $('.cores__link[data-name="Marrom"], .cores__item[data-name="Marrom"]').css('background', '#964D06');
        $('.cores__link[data-name="Bege"], .cores__item[data-name="Bege"]').css('background', '#DCB483');
        $('.cores__link[data-name="Cinza"], .cores__item[data-name="Cinza"]').css('background', '#616161');
        $('.cores__link[data-name="Vinho"], .cores__item[data-name="Vinho"]').css('background', '#D0011B');
        $('.cores__link[data-name="Lilás"], .cores__item[data-name="Lilás"]').css('background', '#C6A2FF');

        $('.menu__dropdown--cores-e-temas .flex-colors').flexslider({
            animation: "slide",
            animationLoop: false,
            controlNav: false,
            itemWidth: 50,
            itemMargin: 10,
            minItems: 14,
            maxItems: 25
        });
    });


    // themes
    
    $('.temas__list').empty();
    var itemId = 0;

    themes.themes.forEach(function(theme, index){
        if (index % 7 === 0) {
            itemId = index;
            $('.temas__list').append('<li class="temas__item temas__item--' + itemId + '">');
        };

        $('.temas__item--' + itemId).append('<a href="' + theme.link + '" class="temas__link">' + theme.name + '</a>');

        if (index % 7 === 0) {
            $('.temas__list').append('</li>');
        };

        $('.product-links__themes').append('<a href="' + theme.link + '" class="product-links__link">' + theme.name + '</a>');
    });

    $('.flex-themes').flexslider({
        animation: "slide",
        animationLoop: false,
        controlNav: false,
        itemWidth: 200,
        itemMargin: 15,
        minItems: 3,
        maxItems: 6
    });
});