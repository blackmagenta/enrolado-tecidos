define(['helpers', '../views/ambient-simulator/page', '../views/ambient-simulator/categories', '../views/ambient-simulator/thumbnails', '../views/ambient-simulator/simulation'], function(helpers){
    var getSimulations = function(category){
        return $.ajax({
            crossDomain: true,
            url: "/api/dataentities/SA/search?categoria=" + category,
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/vnd.vtex.ds.v10+json",
                "REST-Range": "resources=0-49",
                "X-VTEX-API-AppKey": "vtexappkey-enroladotecidos-KVZFFN",
                "X-VTEX-API-AppToken": "PJYUHXZOWTMZGTPAHTZOURAOONKQSUZGMNKDFPSYVFQSDODPVHQRKGKYIKFTMIWCTFMSIMVSBASOBIKAJVVKDIWFCMVVKZLMJNZYCLHJMYQJOVWZZFJRNMZKJGPVQAIB"
            }
        }).then(function(response) {
            return response;
        }).fail(function(response){
            return response;
        });
    };

    var getSimulation = function(id){
        return $.ajax({
            crossDomain: true,
            url: "/api/dataentities/SA/search?id=" + id,
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/vnd.vtex.ds.v10+json",
                "REST-Range": "resources=0-49",
                "X-VTEX-API-AppKey": "vtexappkey-enroladotecidos-KVZFFN",
                "X-VTEX-API-AppToken": "PJYUHXZOWTMZGTPAHTZOURAOONKQSUZGMNKDFPSYVFQSDODPVHQRKGKYIKFTMIWCTFMSIMVSBASOBIKAJVVKDIWFCMVVKZLMJNZYCLHJMYQJOVWZZFJRNMZKJGPVQAIB"
            }
        }).then(function(response) {
            return response;
        }).fail(function(response){
            return response;
        });
    };

    if ($('body').hasClass('ambient-simulator')) {
        $.ajax({
            crossDomain: true,
            url: "/api/dataentities/CS/search",
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/vnd.vtex.ds.v10+json",
                "REST-Range": "resources=0-49",
                "X-VTEX-API-AppKey": "vtexappkey-enroladotecidos-KVZFFN",
                "X-VTEX-API-AppToken": "PJYUHXZOWTMZGTPAHTZOURAOONKQSUZGMNKDFPSYVFQSDODPVHQRKGKYIKFTMIWCTFMSIMVSBASOBIKAJVVKDIWFCMVVKZLMJNZYCLHJMYQJOVWZZFJRNMZKJGPVQAIB"
            }
        }).then(function(categories){
            var selectedCategory;

            dust.render('ambient-simulator/categories', { categories: categories }, function(err, out){
                $('.ambient-simulator__select').html(out);
            });

            if (helpers.getUrlParameter('id')) {
                getSimulation(helpers.getUrlParameter('id')).then(function(result){
                    selectedCategory = result[0].categoria;

                    var simulation = {
                        id: result[0].id,
                        name: result[0].nome,
                        thumbnail: '//enroladotecidos.vtexcrm.com.br/DynamicForm/GetFile?dataEntityInstanceId=SA-' + result[0].id + '&fileName=' + result[0].miniatura,
                        image: '//enroladotecidos.vtexcrm.com.br/DynamicForm/GetFile?dataEntityInstanceId=SA-' + result[0].id + '&fileName=' + result[0].imagem,
                        url: result[0].link
                    };

                    helpers.searchProduct(simulation.url).then(function(results){
                        simulation.sku = results[0].items[0].itemId;

                        dust.render('ambient-simulator/simulation', { simulation: simulation }, function(err, out){
                            $('.simulation__box').addClass('simulation__box--active').html(out);
                        });
                    });

                    $('.ambient-simulator__select option[value="' + selectedCategory + '"]').prop('selected', true);

                    getSimulations(selectedCategory).then(function(results){
                        var simulations = [];
                        
                        results.forEach(function(result){
                            var simulation = {
                                id: result.id,
                                name: result.nome,
                                thumbnail: '//enroladotecidos.vtexcrm.com.br/DynamicForm/GetFile?dataEntityInstanceId=SA-' + result.id + '&fileName=' + result.miniatura,
                                image: '//enroladotecidos.vtexcrm.com.br/DynamicForm/GetFile?dataEntityInstanceId=SA-' + result.id + '&fileName=' + result.imagem,
                                url: result.link
                            }
    
                            simulations.push(simulation);
                        });
    
                        dust.render('ambient-simulator/thumbnails', { simulations: simulations}, function(err, out){
                            $('.ambient-simulator__box-options').html(out);
    
                            $('.other-designs__item a').click(function(e){
                                e.preventDefault();
    
                                $('.simulation__box').addClass('simulation__box--active');
    
                                var selectedSimulationIndex = simulations.map(function(simulation){
                                    return simulation.id;
                                }).indexOf($(this).attr('href'));
    
                                helpers.searchProduct(simulations[selectedSimulationIndex].url).then(function(results){
                                    simulations[selectedSimulationIndex].sku = results[0].items[0].itemId;
    
                                    dust.render('ambient-simulator/simulation', { simulation: simulations[selectedSimulationIndex] },function(err, out){
                                        console.log({ simulation: simulations[selectedSimulationIndex] });
                                        $('.simulation__box').html(out);
                                    });
                                });
                            });
                        });
                    });
                });
            };

            $('.ambient-simulator__select').change(function(){
                selectedCategory = $(this).val();
    
                getSimulations(selectedCategory).then(function(results){
                    var simulations = [];
                    
                    results.forEach(function(result){
                        var simulation = {
                            id: result.id,
                            name: result.nome,
                            thumbnail: '//enroladotecidos.vtexcrm.com.br/DynamicForm/GetFile?dataEntityInstanceId=SA-' + result.id + '&fileName=' + result.miniatura,
                            image: '//enroladotecidos.vtexcrm.com.br/DynamicForm/GetFile?dataEntityInstanceId=SA-' + result.id + '&fileName=' + result.imagem,
                            url: result.link
                        }

                        simulations.push(simulation);
                    });

                    dust.render('ambient-simulator/thumbnails', { simulations: simulations}, function(err, out){
                        $('.ambient-simulator__box-options').html(out);

                        $('.other-designs__item a').click(function(e){
                            e.preventDefault();

                            $('.simulation__box').addClass('simulation__box--active');

                            var selectedSimulationIndex = simulations.map(function(simulation){
                                return simulation.id;
                            }).indexOf($(this).attr('href'));

                            helpers.searchProduct(simulations[selectedSimulationIndex].url).then(function(results){
                                simulations[selectedSimulationIndex].sku = results[0].items[0].itemId;

                                dust.render('ambient-simulator/simulation', { simulation: simulations[selectedSimulationIndex] },function(err, out){
                                    console.log({ simulation: simulations[selectedSimulationIndex] });
                                    $('.simulation__box').html(out);
                                });
                            });
                        });
                    });
                });
            });
        }).fail(function(response){
            console.log(response);
        });
    };
});