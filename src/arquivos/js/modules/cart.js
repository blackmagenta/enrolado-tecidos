define(['helpers', '../views/cart/item', '../views/cart/order-total'], function(helpers){
    var checkoutButton = $('.cart__checkout'),
        miniCart = $('.mini-cart__quantity');

    var checkOrderForm = function(){
        vtexjs.checkout.getOrderForm()
        .then(function(orderForm){
            if (orderForm.items.length) {
                miniCart.html(orderForm.items.length);

                if (orderForm.items.length >= 2) {
                    $('.cart__banner').fadeOut();
                };
        
                orderForm.items.forEach(function(item) {
                    item.imageUrl = item.imageUrl.replace('-55-55', '-128-128');
                    item.priceFormatted = helpers.formatPrice(item.price);
                    item.sellingPriceFormatted = helpers.formatPrice(item.sellingPrice);
                    item.quantitySelector = true;

                    helpers.searchProduct(item.detailUrl).done(function(results){
                        item.availableQuantity = results[0].items[0].sellers[0].commertialOffer.AvailableQuantity;

                        if (results[0]['Tipo de Seletor']) {
                            item.selector = results[0]['Tipo de Seletor'][0];
                        };
            
                        if (item.selector === 'Seletor de Metro') {
                            item.meterSelector = true;
                            item.priceFormatted += '/m';
                            item.sellingPriceFormatted += '/m';
                        } else if (item.selector === 'SKU (exemplo tamanho e cor)') {
                            item.skuSelector = true;
                        };

                        if (item.sellingPrice < item.price) {
                            item.wholeSale = true;
                        };

                        var data = {
                            items: [item]
                        };
                        
                        dust.render('cart/item', data, function(err, out){
                            $('.cart__items').append(out).fadeIn();

                            if (orderForm.items.length >= 2) {
                                $('.cart__items').css('padding-bottom', '287px');
                            };
                        });
                    });
                });

                updateOrderTotal(orderForm);
            };
        });
    };

    var addItem = function(item){
        $('.cart').toggleClass('cart--open', true);

        vtexjs.checkout.getOrderForm()
        .done(function(orderForm) {
            var productIndex = orderForm.items.map(function(item) {
                return item.id;
            }).indexOf(String(item.id));

            if (orderForm.items && productIndex != -1) {
                helpers.alert('.cart__header', 'danger', 'Este produto já está em seu carrinho.');
            } else {
                vtexjs.checkout.addToCart([item], null, 1)
                .done(function(orderForm){
                    if (orderForm.items) {
                        miniCart.html(orderForm.items.length);

                        if (orderForm.items.length >= 2) {
                            $('.cart__banner').fadeOut();
                        };

                        var itemIndex = orderForm.items.map(function(item) {
                            return item.id;
                        }).indexOf(String(item.id));

                        orderForm.items[itemIndex].imageUrl = orderForm.items[itemIndex].imageUrl.replace('-55-55', '-128-128');
                        orderForm.items[itemIndex].priceFormatted = helpers.formatPrice(orderForm.items[itemIndex].price);
                        orderForm.items[itemIndex].sellingPriceFormatted = helpers.formatPrice(orderForm.items[itemIndex].sellingPrice);

                        helpers.searchProduct(orderForm.items[itemIndex].detailUrl)
                        .done(function(results){
                            orderForm.items[itemIndex].availableQuantity = results[0].items[0].sellers[0].commertialOffer.AvailableQuantity;

                            if (results[0]['Tipo de Seletor']) {
                                orderForm.items[itemIndex].selector = results[0]['Tipo de Seletor'][0];
                            };
                
                            if (orderForm.items[itemIndex].selector === 'Seletor de Metro') {
                                orderForm.items[itemIndex].meterSelector = true;
                                orderForm.items[itemIndex].priceFormatted += '/m';
                                orderForm.items[itemIndex].sellingPriceFormatted += '/m';
                            } else if (orderForm.items[itemIndex].selector === 'SKU (exemplo tamanho e cor)') {
                                orderForm.items[itemIndex].skuSelector = true;
                            };

                            if (orderForm.items[itemIndex].sellingPrice < orderForm.items[itemIndex].price) {
                                orderForm.items[itemIndex].wholeSale = true;
                            };

                            var data = {
                                items: [orderForm.items[itemIndex]]
                            };
                            
                            dust.render('cart/item', data, function(err, out){
                                $('.cart__items').append(out).fadeIn();

                                if (orderForm.items.length >= 2) {
                                    $('.cart__items').css('padding-bottom', '327px');
                                };
                            });
                        });
                    } else {
                        helpers.alert('.cart__header', 'danger', 'Erro ao adicionar item, por favor tente novamente.');
                    };
                }).fail(function(){
                    helpers.alert('.cart__header', 'danger', 'Erro ao adicionar item, por favor tente novamente.');
                });
            };
        });
    };

    var updateItem = function(item){
        var itemIndex = 0;
        
        vtexjs.checkout.getOrderForm()
        .then(function(orderForm) {
            itemIndex = orderForm.items.map(function(item) {
                return item.id;
            }).indexOf(String(item.id));

            item.index = itemIndex;

            return vtexjs.checkout.updateItems([item], null, false);
        }).done(function(orderForm) {
            var itemContainer = $('#cart-item--' + item.id),
                priceContainer = itemContainer.find('.cart-item__meter-price'),
                priceDiscountClass = 'cart-item__meter-price--discount';
            
            item.sellingPriceFormatted = helpers.formatPrice(orderForm.items[itemIndex].sellingPrice);

            priceContainer.html(item.sellingPriceFormatted);

            if (orderForm.items[itemIndex].sellingPrice < orderForm.items[itemIndex].price) {
                priceContainer.addClass(priceDiscountClass);
            } else {
                priceContainer.removeClass(priceDiscountClass);
            };

            helpers.alert('.cart__header', 'success', 'Carrinho atualizado.');
        }).fail(function(){
            helpers.alert('.cart__header', 'danger', 'Não foi possível atualizar o carrinho.');
        });
    };

    var removeItem = function(itemId){
        vtexjs.checkout.getOrderForm()
        .then(function(orderForm) {
            itemIndex = orderForm.items.map(function(item) {
                return item.id;
            }).indexOf(String(itemId));

            var item = {
                index: itemIndex,
                quantity: 0
            };
            
            return vtexjs.checkout.removeItems([item], null, 1);
        }).done(function(orderForm) {
            $('#cart-item--' + itemId).slideUp(350, function(){
                $(this).remove();

                if (!orderForm.items.length) {
                    $('.cart__items').fadeOut();
                    checkoutButton.toggleClass('cart__checkout--active', false);
                    miniCart.html('0');

                    $('.cart__banner').fadeIn();
                    $('.cart').toggleClass('cart--open', false);
                } else {
                    helpers.alert('.cart__header', 'success', 'Item removido.');

                    if (orderForm.items.length >= 2) {
                        $('.cart__banner').fadeOut();
                    } else {
                        $('.cart__banner').fadeIn();
                    };
                };
            });
        }).fail(function(orderForm){
            helpers.alert('.cart__header', 'danger', 'Não foi possível remover o item do carrinho.');
        });
    };

    var updateOrderTotal = function(orderForm){
        var cartTotal = $('.cart-total');

        if (cartTotal) {
            cartTotal.remove();
        };

        if (orderForm.items.length) {
            var totalizers = orderForm.totalizers,
                orderTotal = {};
            
            var subtotalTotalizer = totalizers.map(function(totalizer) {
                return totalizer.id;
            }).indexOf('Items');

            var discountsTotalizer = totalizers.map(function(totalizer) {
                return totalizer.id;
            }).indexOf('Discounts');

            var shippingTotalizer = totalizers.map(function(totalizer) {
                return totalizer.id;
            }).indexOf('Shipping');

            if (subtotalTotalizer != -1) {
                orderTotal.subtotal = helpers.formatPrice(totalizers[subtotalTotalizer].value);
            };

            if (discountsTotalizer != -1) {
                orderTotal.discounts = helpers.formatPrice(totalizers[discountsTotalizer].value).replace('-', '');
            };

            if (shippingTotalizer != -1) {
                var shippingInfo = {};

                shippingInfo.postalCode = orderForm.shippingData.address.postalCode;
                shippingInfo.slas = orderForm.shippingData.logisticsInfo[0].slas;
                shippingInfo.selectedSla = orderForm.shippingData.logisticsInfo[0].selectedSla;
                
                shippingInfo.selectedSlaIndex = shippingInfo.slas.map(function(sla){ 
                    return sla.id
                }).indexOf(shippingInfo.selectedSla);

                shippingInfo.slas[shippingInfo.selectedSlaIndex].isSelected = true;
                shippingInfo.estimate = 'Até ' + shippingInfo.slas[shippingInfo.selectedSlaIndex].shippingEstimate.replace('bd', ' Dias');
                shippingInfo.total = helpers.formatPrice(totalizers[shippingTotalizer].value);
                orderTotal.shippingInfo = shippingInfo;
            };

            orderTotal.total = helpers.formatPrice(orderForm.value);

            dust.render('cart/order-total', { orderTotal: orderTotal}, function(err, out){
                $('.cart__footer').prepend(out).fadeIn();
                checkoutButton.toggleClass('cart__checkout--active', true);
            });
        } else {
            if (cartTotal) {
                cartTotal.remove();
            };
        };
    };


    $('[data-toggle="cart"]').click(function(e){
        e.preventDefault();
        $('.cart').toggleClass('cart--open');
    });

    checkOrderForm();


    $(document).on('click', '.btn--buy', function(e){
        e.preventDefault();

        var item = {
            id: $(this).data('sku'),
            quantity: $(this).data('quantity'),
            seller: $(this).data('seller')
        };

        addItem(item);
    });


    $(document).on('change', '.cart-item__meter-selector', function(){
        updateItemQuantity($(this).data('id'), Number($(this).val()));
    });

    var updateItemQuantity = function(id, quantity){
        var delayTimer;

        clearTimeout(delayTimer);

        delayTimer = setTimeout(function() {
            updateItem({
                id: id,
                quantity: quantity
            });
        }, 2500);
    };

    $(document).on('click', '.quantity__add', function(){
        var quantityInput = $('.cart-item__meter-selector[data-id="' + $(this).data('id') + '"]'),
            quantityMin = Number(quantityInput.attr('min'));
            quantityMax = Number(quantityInput.attr('max'));

        quantityInput.val(function(i, oldval){
            if (oldval < quantityMax) {
                return ++oldval;
            } else {
                return oldval;
            };
        });

        updateItemQuantity(quantityInput.data('id'), Number(quantityInput.val()));
    });

    $(document).on('click', '.quantity__remove', function(){
        var quantityInput = $('.cart-item__meter-selector[data-id="' + $(this).data('id') + '"]'),
            quantityMin = Number(quantityInput.attr('min'));
            quantityMax = Number(quantityInput.attr('max'));

        quantityInput.val(function(i, oldval){
            if (oldval > quantityMin) {
                return --oldval;
            } else {
                return oldval;
            };
        });

        updateItemQuantity(quantityInput.data('id'), Number(quantityInput.val()));
    });


    $(document).on('click', '.cart-item__remove', function(){
        var itemId = $(this).parent().attr('id').replace('cart-item--', '');

        removeItem(itemId);
    });


    $(window).on('orderFormUpdated.vtex', function(evt, orderForm) {
        updateOrderTotal(orderForm);
    });


    $(document).on('click', '.shipping-couriers__form .form-check', function(){
        var selectedCourier = $(this).find('input').val();

        vtexjs.checkout.getOrderForm()
        .then(function(orderForm) {
            orderForm.shippingData.logisticsInfo.forEach(function(logisticInfo){
                logisticInfo.selectedSla = selectedCourier;
            });

            vtexjs.checkout.sendAttachment('shippingData', orderForm.shippingData);
            vtexjs.checkout.clearMessages();
            updateOrderTotal(orderForm);
        });
    });
});