define(['helpers', '../views/search/results'], function(helpers){
    var searchInput = $('.search .search__input'),
        searchResultsContainer = $('.search .search__results'),
        searchButton = $('.search__btn'),
        delayTimer;

    var validateInput = function(term) {
        if (term.length > 2 && term !== '' && term.length < 50) {
            return true;
        } else {
            return false;
        };
    };

    var formatResults = function(term, results){
        results.forEach(function(result) {
            result.nameHighlighted = result.productName;
            result.link = result.link;
            result.imageId = result.items[0].images[0].imageId;
            result.imageUrl = result.items[0].images[0].imageUrl.replace(result.imageId, result.imageId+'-30-30');
        });

        return results;
    };

    var goToSearchPage = function(term) {
        window.location = location.protocol + '//' + location.hostname + '/busca?ft=' + term;
    };

    searchInput.keyup(function(e){
        var term = $(this).val();

        $(this).val(term);
        searchResultsContainer.addClass('search__results--loading');

        if (validateInput(term)) {
            clearTimeout(delayTimer);
            
            if (e.which == 13) {
                goToSearchPage(term);
            };

            delayTimer = setTimeout(function() {
                helpers.searchProduct('?ft=' + term + '&_from=0&_to=6')
                .then(function(results, status, response){
                    if (results.length) {
                        dust.render('search/results', { results: formatResults(term, results) }, function(err, out){
                            searchResultsContainer.html(out).addClass('show').removeClass('search__results--loading');
                            searchInput.addClass('search__input--active');
                        });
                    } else {
                        searchResultsContainer.empty().removeClass('show search__results--loading');
                        searchInput.removeClass('search__input--active');
                    };
                });
            }, 750);
        } else {
            searchResultsContainer.empty().removeClass('show');
            searchInput.removeClass('search__input--active');
        };
    });


    searchButton.click(function(){
        var term = $(this).parent().find('.search__input').val();

        if (validateInput(term)) {
            goToSearchPage(term);
        };
    });

    searchInput.on('click focus', function(e){
        e.stopPropagation();
        
        if (validateInput($(this).val()) && !searchResultsContainer.hasClass('show') ) {
            searchResultsContainer.addClass('show');
            searchInput.addClass('search__input--active');
        };
    });
    
    searchResultsContainer.click(function(e){
        e.stopPropagation();
    });

    $(window).click(function() {
        if (searchResultsContainer.hasClass('show')) {
            searchResultsContainer.removeClass('show');
            searchInput.removeClass('search__input--active');
        };
    });
});