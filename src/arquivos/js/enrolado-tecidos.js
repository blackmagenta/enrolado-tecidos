define([
    'helpers',
    'menu',
    'search',
    'shipping-info',
    'cart',
    'contact-form',
    'homepage',
    'catalog',
    'product-page',
    'ambient-simulator',
    'meter-calculator'
], function(){
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        $('body').addClass('ie');
    }

    $('[href="#chat"]').click(function(){
        $zopim(function() {
            $zopim.livechat.window.show();
        });
    });

    $('[data-toggle="pill"]').click(function(){
        history.pushState(null, '', $(this).attr('href'));
    });

    if ($('body').hasClass('information-central') && window.location.href.indexOf('#') != -1) {
        var activeContent = window.location.href.split('#')[1];

        $('[data-toggle="pill"], .tab-pane').removeClass('active show');
        $('[href="#' + activeContent + '"], #' + activeContent + '').addClass('active show');

        $('html, body').scrollTop(0);
    };

    if ($('body').hasClass('orders')) {
        $('link[href="//io.vtex.com.br/front-libs/bootstrap/2.3.2/css/bootstrap.min.css"]').attr('disabled', true);

        var hideSubscriptions = function(){
            if ($('a:contains("Minhas Recorrências")')) {
                clearInterval(checkDeposit);

                $('a:contains("Minhas Recorrências")').hide();
            };
        };
        
        var checkDeposit = setInterval(hideSubscriptions, 100);
    };

    $('.notifyme-button-ok').val('Me Avise!');
});